package org.uniroma2.tesi.codd;

public class ODDException extends Exception {

	private static final long serialVersionUID = 7393264134298910164L;

	public ODDException() {}

	public ODDException(String message) {
		super(message);
	}

	public ODDException(Throwable cause) {
		super(cause);
	}

	public ODDException(String message, Throwable cause) {
		super(message, cause);
	}

	public ODDException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
