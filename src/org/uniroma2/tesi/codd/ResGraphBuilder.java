package org.uniroma2.tesi.codd;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.uniroma2.tesi.codd.metricsprovider.ResourceMetricsProvider;
import org.uniroma2.tesi.codd.model.Pair;
import org.uniroma2.tesi.codd.model.ResourceEdge;
import org.uniroma2.tesi.codd.model.ResourceGraph;
import org.uniroma2.tesi.codd.model.ResourceVertex;
import org.uniroma2.tesi.codd.model.ServiceEdge;
import org.uniroma2.tesi.codd.model.ServiceGraph;
import org.uniroma2.tesi.codd.model.ServiceVertex;

public class ResGraphBuilder {

	protected TYPE type;
	protected ResourceGraph resGraph; 

	protected ServiceGraph serGraph; 
	private int availableResourceMultiplier;
	
	private int NUMBER_SERVICES = 5;
	private int NUMBER_NODES = 3;
	
	protected LinkedList<String> servicesList =  new LinkedList<String>();
	protected LinkedList<String> nodesList =  new LinkedList<String>();
	
	public enum TYPE {
		FULL_MESH, SERVICE_POD, UNKNOWN, CUSTOM, EXTERNAL
	};

	
	public ResGraphBuilder() {
		this.type = TYPE.UNKNOWN;
		resGraph = null;
	}
	public ResGraphBuilder(ResourceGraph resourceGraph) {
		this.resGraph = resourceGraph;
		this.type = TYPE.UNKNOWN;
	}
	public ResGraphBuilder(int availableResourceMultiplier) {
		resGraph = null;
		this.availableResourceMultiplier = availableResourceMultiplier;
	}
		
	public ResourceGraph getGraph(){
		return resGraph;
	}

	public ServiceGraph getServiceGraph(){
		return serGraph;
	}
	
	public ResGraphBuilder 	create(
			ResourceMetricsProvider metricsProvider,
			TYPE type,
			int numResources
			){

		this.type = type;
				
		switch (type) {
		case FULL_MESH:
			return createFullMesh(metricsProvider, numResources);
		
		case SERVICE_POD:
			return createServicePod(metricsProvider, numResources);
		case CUSTOM:
			return createCustomServicePod(metricsProvider, numResources);
	//	case MULTIHOP: 
			//return testMultihop();
		
		//case BRITE:
		//	return createBrite(metricsProvider, numResources);
			
		default:
			//return createAnsnet(metricsProvider, numResources);	
			return createFullMesh(metricsProvider, numResources);
		}
	}
	
	
	public ResourceGraph getSubset(List<Integer> resIds){
		
		if (resGraph == null)
			return null;
		
		Map<Integer, ResourceVertex> sNodes = new HashMap<Integer, ResourceVertex>();
		Map<Pair, ResourceEdge> sEdges = new HashMap<Pair, ResourceEdge>();

		/* Extract all nodes */
		for (Integer id : resIds){
			sNodes.put(id, resGraph.getVertices().get(id));
		}
		
		/* Extract all vertices */
		for (ResourceVertex uRes : sNodes.values()){
			int u = uRes.getIndex();
			for (ResourceVertex vRes : sNodes.values()){
				int v = vRes.getIndex();
				
				Pair uv = new Pair(u, v);
				sEdges.put(uv, resGraph.getEdges().get(uv));
			}	
		}

		return new ResourceGraph("excerpt", sNodes, sEdges);
		
	}
	
	public static ResourceGraph getSubset(ResourceGraph resourceGraph, List<Integer> resIds){
		
		if (resourceGraph == null)
			return null;
		
		Map<Integer, ResourceVertex> sNodes = new HashMap<Integer, ResourceVertex>();
		Map<Pair, ResourceEdge> sEdges = new HashMap<Pair, ResourceEdge>();

		/* Extract all nodes */
		for (Integer id : resIds){
			sNodes.put(id, resourceGraph.getVertices().get(id));
		}
		
		/* Extract all vertices */
		for (ResourceVertex uRes : sNodes.values()){
			int u = uRes.getIndex();
			for (ResourceVertex vRes : sNodes.values()){
				int v = vRes.getIndex();
				
				Pair uv = new Pair(u, v);
				sEdges.put(uv, resourceGraph.getEdges().get(uv));
			}	
		}

		return new ResourceGraph("excerpt", sNodes, sEdges);
		
	}
	
	public TYPE getType() {
		return type;
	}
	public void setType(TYPE type) {
		this.type = type;
	}
	
	public static Map<Integer, ServiceVertex> createExternalServiceVertexMap(
			LinkedList<ServiceVertex> serviceVertexList){
		Map<Integer, ServiceVertex> vRes = new HashMap<Integer, ServiceVertex>();
		for(int index = 0; index< serviceVertexList.size(); index++){
			vRes.put(new Integer(index), serviceVertexList.get(index));
		}
		return vRes;
	}
	
	public static Map<Pair, ServiceEdge> createExternalServiceEdgeMap(
			LinkedList<ServiceEdge> serviceEdgeList){
		Map<Pair, ServiceEdge> eRes = new HashMap<Pair, ServiceEdge>();
		for(int index = 0; index< serviceEdgeList.size(); index++){
			ServiceEdge e = serviceEdgeList.get(index);
			eRes.put(new Pair(e.getFrom(), e.getTo()), e);
		}
		return eRes;
	}
		
	
	public ResGraphBuilder createExternalServicePod(
			Map<Integer, ServiceVertex> vRes,
			Map<Pair, ServiceEdge> eRes
			){
		
		String graphName = "services";
		serGraph = new ServiceGraph(graphName, vRes, eRes);
		return this;
		
	}
	
	private ResGraphBuilder createCustomServicePod(
			ResourceMetricsProvider metricsProvider,
			int numResources
			){
			Map<Integer, ServiceVertex> vRes = new HashMap<Integer, ServiceVertex>();
			Map<Pair, ServiceEdge> eRes = new HashMap<Pair, ServiceEdge>();
			
			//service 0 node 0
			createService(0, 0, 0, vRes);

			//service 1 node 1
			createService(1, 1, 1, vRes);
			
			//service 2 node 2
			createService(2, 2, 2, vRes);
			
			//service 3 node 0
			createService(3, 0, 3, vRes);
			
			//service 4 node 1
			createService(4, 1, 4, vRes);
			
			//service 4 node 0
			createService(4, 0, 5, vRes);
			
			//service 3 node 2
			createService(3, 1, 6, vRes);
			
			//service 0 node 2
			createService(0, 2, 7, vRes);
			
			//service 4 node 2
			createService(4, 2, 8, vRes);
			
			//service 3 node 2
			createService(3, 2, 9, vRes);
			
			/* 2. Create DspEdges to represent link between executors */
			for (ServiceVertex u : vRes.values()){
				for (ServiceVertex v : vRes.values()){
					int uIndex = u.getIndex();
					int vIndex = v.getIndex();

					int latUV = (int) metricsProvider.getSerLatency(u.getNode(), v.getNode());// getLat(u.getNode(), v.getNode()); //(int) metricsProvider.getLatency(uIndex, vIndex);
					double bigAUV = 1.0; // metricsProvider.getAvailability(uIndex, vIndex);
					
					ServiceEdge rEdge = new ServiceEdge(
							uIndex, 
							vIndex, latUV, bigAUV);
					eRes.put(new Pair(uIndex, vIndex), rEdge);
				}
			}


			/* 3. Creating object DspGraph */
			String graphName = "services";
			serGraph = new ServiceGraph(graphName, vRes, eRes);
			return this;
			
	}
	private void createService(int service, int n, int index, 
			Map<Integer, ServiceVertex> vRes){
		
		String type = "Service" + Integer.toString(service);
		String node = "Node" + Integer.toString(n);
		
	//	int cu 			= 1;//metricsProvider.getAvailableResources(index);

		double su 		= 1.0; //metricsProvider.getSpeedup(index);

		double bigAu	= 1.0 ;//metricsProvider.getAvailability(index);
		
		ServiceVertex v = new ServiceVertex(
				index,
				Integer.toString(index),
				type,
				node,
				"192.168.1." + index,
				su,
				bigAu);
		vRes.put(new Integer(index), v);
		
	}
	public static ServiceVertex createService(int service, int n, int index, 
			double availability, int tu){
		
		String type = "Service" + Integer.toString(service);
		String node = "Node" + Integer.toString(n);
		
	//	int cu 			= 1;//metricsProvider.getAvailableResources(index);

		double su 		= 1.0; //metricsProvider.getSpeedup(index);

		double bigAu	= availability ;//metricsProvider.getAvailability(index);
		
		ServiceVertex v = new ServiceVertex(
				index,
				Integer.toString(index),
				type,
				node,
				"192.168.1." + index,
				su,
				bigAu,
				tu);
		return v;
	}
	
	private ResGraphBuilder createServicePod(
			ResourceMetricsProvider metricsProvider,
			int numResources
			){

		Map<Integer, ServiceVertex> vRes = new HashMap<Integer, ServiceVertex>();
		Map<Pair, ServiceEdge> eRes = new HashMap<Pair, ServiceEdge>();
		setServicesList(NUMBER_SERVICES);
		setNodesList(NUMBER_NODES);

		/* 1. Create ResourceVertex to represent nodes (supervisors, set of worker slots) */
		for(int index = 0; index < numResources; index++){
			// non so perché ma è sbagliato e ne pesca 2 invece di 1.. 
			// ora li ho messi manualmente
			
			String type = randType(index);
			String node = randNode(index);
			
		//	int cu 			= 1;//metricsProvider.getAvailableResources(index);

			double su 		= 1.0; //metricsProvider.getSpeedup(index);

			double bigAu	= 1.0 ;//metricsProvider.getAvailability(index);
			
			ServiceVertex v = new ServiceVertex(
					index,
					Integer.toString(index),
					type,
					node,
					"192.168.1." + index,
					su,
					bigAu);
			vRes.put(new Integer(index), v);
		}


		/* 2. Create DspEdges to represent link between executors */
		for (ServiceVertex u : vRes.values()){
			for (ServiceVertex v : vRes.values()){
				int uIndex = u.getIndex();
				int vIndex = v.getIndex();

				int latUV = (int) metricsProvider.getSerLatency(u.getNode(), v.getNode());// getLat(u.getNode(), v.getNode()); //(int) metricsProvider.getLatency(uIndex, vIndex);
				double bigAUV = 1.0; // metricsProvider.getAvailability(uIndex, vIndex);
				
				ServiceEdge rEdge = new ServiceEdge(
						uIndex, 
						vIndex, latUV, bigAUV);
				eRes.put(new Pair(uIndex, vIndex), rEdge);
			}
		}


		/* 3. Creating object DspGraph */
		String graphName = "services";
		serGraph = new ServiceGraph(graphName, vRes, eRes);
		return this;

	}
	
	private ResGraphBuilder createFullMesh(
			ResourceMetricsProvider metricsProvider,
			int numResources
			){

		Map<Integer, ResourceVertex> vRes = new HashMap<Integer, ResourceVertex>();
		Map<Pair, ResourceEdge> eRes = new HashMap<Pair, ResourceEdge>();


		/* 1. Create ResourceVertex to represent services, pod on K8S 
		 */
		for(int index = 0; index < numResources; index++){
			// non so perché ma è sbagliato e ne pesca 2 invece di 1.. 
			// ora li ho messi manualmente
			//String type = randType(index);
			//questi valori non servono
			
			int cu 			= 1;//metricsProvider.getAvailableResources(index);
			double su 		= metricsProvider.getSpeedup(index);
			double bigAu	= metricsProvider.getAvailability(index);
			double instanceLaunchTime = metricsProvider.getInstanceLaunchTime(index);
			double uploadRateToDS = metricsProvider.getUploadRateToDS(index);
			double downloadRateFromDS = metricsProvider.getDownloadRateFromDS(index);
			double uploadRateToLocalDS = metricsProvider.getUploadRateToLocalDS(index);
			double downloadRateFromLocalDS = metricsProvider.getDownloadRateFromLocalDS(index);
			double dataStoreRTT = metricsProvider.getDataStoreRTT(index);
			
			//devo introdurre un modo per distinguere le risorse concrete e i nodi su 
			//cui si trovano
			
			ResourceVertex v = new ResourceVertex(index, cu, su, bigAu, instanceLaunchTime, uploadRateToDS, downloadRateFromDS, uploadRateToLocalDS, downloadRateFromLocalDS, dataStoreRTT);
			vRes.put(new Integer(index), v);
		}


		/* 2. Create DspEdges to represent link between executors */
		for (ResourceVertex u : vRes.values()){
			for (ResourceVertex v : vRes.values()){
				int uIndex = u.getIndex();
				int vIndex = v.getIndex();

				int latUV = (int) metricsProvider.getLatency(uIndex, vIndex);
				System.out.println(latUV);
				double bigAUV = metricsProvider.getAvailability(uIndex, vIndex);
				double bwUV = metricsProvider.getBandwidth(uIndex, vIndex);
				double costUV = metricsProvider.getCostPerUnitData(uIndex, vIndex);

				ResourceEdge rEdge = new ResourceEdge(uIndex, vIndex, bwUV, latUV, bigAUV, costUV);
				eRes.put(new Pair(uIndex, vIndex), rEdge);
			}
		}


		/* 3. Creating object DspGraph */
		String graphName = "resources";
		resGraph = new ResourceGraph(graphName, vRes, eRes);
		return this;

	}
	
	public void setServicesList(int n){	
		for(int i=0; i<n; i++)
		{
			servicesList.add("Service"+Integer.toString(i));
		}		
	}
	
public void setNodesList(int n){
		
		for(int i=0; i<n; i++)
		{
			nodesList.add("Node"+Integer.toString(i));
		}
		
	}

	
	public String randType(int i){		
		int index = i % NUMBER_SERVICES;
		return servicesList.get(index) ;
	}
	public String randNode(int i){		
		int index = i % NUMBER_NODES;
		return nodesList.get(index) ;
	}
	
	
	/*public int getLat(String u, String v){
		if(u.equals(v)){
			return 0;
		}
		else return metricsProvider.getLatency(uIndex, vIndex);	
	}*/

	public void printSerGraph(boolean alsoEdges){

		System.out.println("SER Graph");
		System.out.println("* Services ");
		for(ServiceVertex v : serGraph.getVertices().values()){
			System.out.println("   " + v.toString());
		}

		System.out.println();
		System.out.println("* Edges: " + serGraph.getEdges().size());
		if (alsoEdges){
			for(ServiceEdge e : serGraph.getEdges().values()){
				System.out.println("   " + e.toString());
			}
		}
		printDelayMatrix();
		return;

	}
	public void printDelayMatrix(){
		LinkedList<ServiceEdge> edgeList = new LinkedList<ServiceEdge>();
		
		for(ServiceEdge e : serGraph.getEdges().values()){
			edgeList.add(e);
		}
		int size = (int) Math.sqrt(edgeList.size());
		int i;
		int j;
		System.out.println("Delay Matrix :");
		for(i = 0; i< size; ++i){		
			for(j = 0; j< size; ++j){
				ServiceEdge e = getEdge(edgeList, i, j);
				
				System.out.printf("%3d", e.getDelay());
				//System.out.print("   ");
				edgeList.remove(e);				
			}
			System.out.print("\n");
		}
	}
	
	public ServiceEdge getEdge(LinkedList<ServiceEdge> edgeList, int i, int j){
			
			for(ServiceEdge e:edgeList){
				if(e.getFrom() == i && e.getTo()==j){
					return e;
				}
			}
			return null;
	}
		
			
	
	public void printGraph(boolean alsoEdges){

		System.out.println("RES Graph");
		System.out.println("* Vertices ");
		for(ResourceVertex v : resGraph.getVertices().values()){
			System.out.println("   " + v.toString());
		}

		System.out.println();
		System.out.println("* Edges: " + resGraph.getEdges().size());
		if (alsoEdges){
			for(ResourceEdge e : resGraph.getEdges().values()){
				System.out.println("   " + e.toString());
			}
		}

		return;

	}


	public int getMaxExecutorPerSlot() {
		return availableResourceMultiplier;
	}

}
