package org.uniroma2.tesi.codd.model;


public class Microservice {
	private String name;
	private int tu;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getTu() {
		return tu;
	}
	public void setTu(int tu) {
		this.tu = tu;
	}
	public Microservice(String name, int tu) {
		super();
		this.name = name;
		this.tu = tu;
	}

	
}
