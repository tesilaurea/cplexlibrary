package org.uniroma2.tesi.codd.model;

import ilog.concert.IloException;
import ilog.concert.IloModeler;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplexModeler;

public class PlacementY {
	
	protected IloModeler modeler;
	protected IloNumVar Y[][][][];
	
	public PlacementY(MSoaGraph dsp, ResourceGraph res) throws IloException {
		
		/* XXX: to be fixed */
		int resSize = res.getVertices().size();
		for (ResourceVertex uRes : res.getVertices().values()){
			if (uRes.getIndex() >= resSize)
				resSize = uRes.getIndex() + 1;
		}
	
		this.Y = new IloNumVar[dsp.getVertices().size()][dsp.getVertices().size()][resSize][resSize];
//		this.Y = new IloNumVar[dsp.getVertices().size()][dsp.getVertices().size()][res.getVertices().size()][res.getVertices().size()];
		this.modeler = new IloCplexModeler();
		
		/* XXX: you can restrict y_ijuv here... */
		for (MSoaEdge eDsp : dsp.getEdges().values()) {
			int i = eDsp.getFrom();
			int j = eDsp.getTo();
			
			for (ResourceEdge eRes : res.getEdges().values()) {
				int u = eRes.getFrom();
				int v = eRes.getTo();
				
				this.Y[i][j][u][v] = this.modeler.boolVar("y[" + i + "][" + j + "][" + u + "][" + v + "]");
			}
			
//			/* Add self-loop */
//			for (ResourceVertex vRes : res.getVertices().values()) {
//				int u = vRes.getIndex();
//				this.Y[i][j][u][u] = this.modeler.boolVar("y[" + i + "][" + j + "][" + u + "][" + u + "]");
//			}
			
		}		
		
	}
	
	public IloNumVar get(final int i, final int j, final int u, final int v) {
		return this.Y[i][j][u][v];
	}
	@Override
	public String toString() {
		return "y(" + super.toString() + ")";
	}

}
