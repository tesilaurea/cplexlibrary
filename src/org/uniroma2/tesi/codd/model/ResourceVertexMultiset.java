package org.uniroma2.tesi.codd.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.uniroma2.tesi.codd.utils.Multiset;


public class ResourceVertexMultiset {

	protected int index;
	protected int cardinality;
	protected Multiset verticesIndex;
	protected List<ResourceVertex> vertices;
		
	public ResourceVertexMultiset(int index, Multiset resourceIndexes, ResourceGraph resGraph) {

		this.index = index;
		this.verticesIndex = resourceIndexes;
		this.vertices = new ArrayList<ResourceVertex>();
		
		Map<Integer, ResourceVertex> nodes = resGraph.getVertices();
		Iterator<Integer> rInd = resourceIndexes.iterator();
		while (rInd.hasNext()){
			Integer u = rInd.next();
			ResourceVertex uRes = nodes.get(u);
			if (uRes != null){
				vertices.add(uRes);
			}
		}
		
		this.cardinality = vertices.size();
		
	}

	public int getIndex() {
		return index;
	}

	public Multiset getVerticesIndex() {
		return verticesIndex;
	}

	public List<ResourceVertex> getVertices() {
		return vertices;
	}

	public int getCardinality() {
		return cardinality;
	}
	
	public int getMultiplicity(ResourceVertex uRes){
		
		int multiplicity = 0;
		
		for (ResourceVertex vertex : this.vertices){
			if (vertex.equals(uRes)){
				multiplicity++;
			}
		}
		
		return multiplicity;
	}
	
	public boolean equals(List<ResourceVertex> vertices){
		
		if (vertices == null)
			return false;
		
		if (this.vertices.size() != vertices.size())
			return false;
		
		for (int i = 0; i < this.vertices.size(); i++){
			ResourceVertex thisRV = this.vertices.get(i);
			ResourceVertex otherRV = vertices.get(i);
			
			if (thisRV == null || otherRV == null || thisRV.getId() == null || otherRV.getId() == null)
				return false; 
			
			if (!thisRV.getId().equals(otherRV.getId()))
				return false;
		}
		
		return true;
	}

	@Override
	public String toString() {
		
		if (this.vertices == null)
			return "";
		
		return this.vertices.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		ResourceVertexMultiset other = (ResourceVertexMultiset) obj;
		
		return vertices.equals(other.vertices);
	}

	/**
	 * Returns true if other multiset represent a placement which can be obtained from this in 1 step.
	 *
	 * One step means one migration or adding/removing one replica.
	 *
	 * @param other Other multiset.
	 * @return True if the "distance" between multisets is 1.
	 */
	public boolean isAtOneStepFrom (ResourceVertexMultiset other)
	{
		if (this.equals(other))
			return true;

		/*
		 * Case A: same cardinality.
		 */
		if (this.cardinality == other.cardinality) {
			/* At most one node can have different # of replicas in the other. */
			int counter = 0;
			for (ResourceVertex u : this.getVertices()) {
				if (this.getMultiplicity(u) != other.getMultiplicity(u))
					++counter;
			}

			return (counter <= 1);
		}


		/*
		 * Case B: cardinality difference is 1.
		 */
		if (Math.abs(this.cardinality - other.cardinality) == 1) {
			ResourceVertexMultiset smaller, larger;
			if (this.cardinality > other.cardinality) {
				smaller = other;
				larger = this;
			} else {
				smaller = this;
				larger = other;
			}

			for (ResourceVertex u : smaller.getVertices()) {
				if (smaller.getMultiplicity(u) > larger.getMultiplicity(u))
					return false;
			}

			return true;
		}

		/* Case C: cardinality difference is > 1 */
		return false;
	}
	
}
