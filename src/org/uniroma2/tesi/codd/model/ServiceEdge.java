package org.uniroma2.tesi.codd.model;

public class ServiceEdge {

	protected int from;
	protected int to;
	
	protected int delay; 
	protected double availability;
	
	
	public ServiceEdge(int from, int to, int delay, double availability) {
		super();
		this.from = from;
		this.to = to;
		this.delay = delay;
		this.availability = availability;
	}
	

	public int getFrom() {
		return from;
	}


	public void setFrom(int from) {
		this.from = from;
	}


	public int getTo() {
		return to;
	}


	public void setTo(int to) {
		this.to = to;
	}


	public int getDelay() {
		return delay;
	}


	public void setDelay(int delay) {
		this.delay = delay;
	}


	public double getAvailability() {
		return availability;
	}


	public void setAvailability(double availability) {
		this.availability = availability;
	}


	@Override
	public int hashCode() {
		return from * 1000 + to;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		ServiceEdge other = (ServiceEdge) obj;
		
		return (from == other.from && to == other.to);
			
	}


	@Override
	public String toString() {
		return "ServiceEdge [from=" + from + ", to=" + to + ", delay=" + delay + ", availability=" + availability + "]";
	}
	
	
}
