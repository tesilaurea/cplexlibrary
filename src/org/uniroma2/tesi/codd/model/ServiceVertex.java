package org.uniroma2.tesi.codd.model;

import java.util.LinkedList;

public class ServiceVertex {

	protected int index; //è l'indice del service
	protected String id; //potrebbe essere l'ip
	
	protected String type; //è la label con cui si discrimina
	protected String node; // per capire se sono collocati o no
	protected String ip;
	protected double speedup;
	protected double availability;
	protected int tu;
	protected LinkedList<Microservice> microservices;
	
	
	@Override
	public String toString() {
		return "ServiceVertex [index=" + index + ", id=" + id + ", type=" + type + ", node=" + node + ", ip=" + ip
				+ ", speedup=" + speedup + ", availability=" + availability + ", tu=" + tu + "]";
	}
	public ServiceVertex(int index, String id, String type, String node, String ip, double speedup,
			double availability) {
		super();
		this.index = index;
		this.id = id;
		this.type = type;
		this.node = node;
		this.ip = ip;
		this.speedup = speedup;
		this.availability = availability;
	}
	public ServiceVertex(int index, String id, String type, String node, String ip, double speedup,
			double availability, int tu) {
		super();
		this.index = index;
		this.id = id;
		this.type = type;
		this.node = node;
		this.ip = ip;
		this.speedup = speedup;
		this.availability = availability;
		this.tu = tu;
	}
	public ServiceVertex(int index, String id, String type, String node, String ip, double speedup,
			double availability, int tu, LinkedList<Microservice> microservices) {
		super();
		this.index = index;
		this.id = id;
		this.type = type;
		this.node = node;
		this.ip = ip;
		this.speedup = speedup;
		this.availability = availability;
		this.tu = tu;
		this.microservices = microservices;
	}
	
	
	public int getTu() {
		return tu;
	}
	public void setTu(int tu) {
		this.tu = tu;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getNode() {
		return node;
	}
	public void setNode(String node) {
		this.node = node;
	}
	public double getSpeedup() {
		return speedup;
	}
	public void setSpeedup(double speedup) {
		this.speedup = speedup;
	}
	public double getAvailability() {
		return availability;
	}
	public void setAvailability(double availability) {
		this.availability = availability;
	}
	@Override
	public int hashCode() {
		return index;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		ServiceVertex other = (ServiceVertex) obj;

		return index == other.index;
	}
	public LinkedList<Microservice> getMicroservices() {
		return microservices;
	}
	public void setMicroservices(LinkedList<Microservice> microservices) {
		this.microservices = microservices;
	}
	
	
}
