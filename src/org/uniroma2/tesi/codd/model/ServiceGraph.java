package org.uniroma2.tesi.codd.model;

import java.util.Map;

public class ServiceGraph {

	protected String name;
	protected Map<Pair, ServiceEdge> edges;
	protected Map<Integer, ServiceVertex> vertices;
	
	public ServiceGraph(String name, 
			Map<Integer, ServiceVertex> vertices,
			Map<Pair, ServiceEdge> edges) {
		super();
		this.name = name;
		this.edges = edges;
		this.vertices = vertices;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Map<Pair, ServiceEdge> getEdges() {
		return edges;
	}
	public void setEdges(Map<Pair, ServiceEdge> edges) {
		this.edges = edges;
	}
	public Map<Integer, ServiceVertex> getVertices() {
		return vertices;
	}
	public void setVertices(Map<Integer, ServiceVertex> vertices) {
		this.vertices = vertices;
	}
	
	@Override
	public String toString() {
		return "ServiceGraph(" + name + ")";		
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		ServiceGraph other = (ServiceGraph) obj;
		
		if (name != null && vertices != null && edges != null){

			return (name.equals(other.name) &&
					vertices.equals(other.vertices) &&
					edges.equals(other.edges));
			
		}
			
		return false;
			
	}
}
