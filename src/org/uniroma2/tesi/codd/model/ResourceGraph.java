package org.uniroma2.tesi.codd.model;

import java.util.Map;

public class ResourceGraph {

	protected String name;
	protected Map<Pair, ResourceEdge> edges;
	protected Map<Integer, ResourceVertex> vertices;

	protected ResourceGraph lowLevelGraph; 
	protected int hierachyLevel;
	protected double referenceServiceRate;
	
	public ResourceGraph(String name, 
			Map<Integer, ResourceVertex> vertices,
			Map<Pair, ResourceEdge> edges) {
		super();
		this.name = name;
		this.edges = edges;
		this.vertices = vertices;
		this.hierachyLevel = 0;
		this.lowLevelGraph = null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<Pair, ResourceEdge> getEdges() {
		return edges;
	}

	public void setEdges(Map<Pair, ResourceEdge> edges) {
		this.edges = edges;
	}

	public Map<Integer, ResourceVertex> getVertices() {
		return vertices;
	}

	public void setVertices(Map<Integer, ResourceVertex> vertices) {
		this.vertices = vertices;
	}
	
//	public double getReferenceServiceRate() {
//		return referenceServiceRate;
//	}
//
//	public void setReferenceServiceRate(
//			double referenceServiceRate) {
//		this.referenceServiceRate = referenceServiceRate;
//	}

	public int getHierachyLevel() {
		return hierachyLevel;
	}

	public void setHierachyLevel(int hierachyLevel) {
		this.hierachyLevel = hierachyLevel;
	}

	public ResourceGraph getLowLevelGraph() {
		return lowLevelGraph;
	}

	public void setLowLevelGraph(ResourceGraph lowLevelGraph) {
		this.lowLevelGraph = lowLevelGraph;
	}

	@Override
	public String toString() {
		return "ResourceGraph(" + name + ")";		
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		ResourceGraph other = (ResourceGraph) obj;
		
		if (name != null && vertices != null && edges != null){

			return (name.equals(other.name) &&
					vertices.equals(other.vertices) &&
					edges.equals(other.edges));
			
		}
			
		return false;
			
	}
	
	
}
