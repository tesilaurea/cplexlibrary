package org.uniroma2.tesi.codd.model;

import ilog.cplex.IloCplex;

import java.util.HashMap;
import java.util.Map;

public class OptimalMultisetSolution extends OptimalSolution{

	
	protected Map<Integer, ResourceVertexMultiset> dspToRes;
	protected Map<Integer, ResourceVertexMultiset> allMultisets;
	
	public OptimalMultisetSolution(int numberOfDspVertices, Map<Integer, ResourceVertexMultiset> allMultisets) {

		super(numberOfDspVertices);
		
		this.allMultisets = allMultisets;
		
		dspToRes = new HashMap<Integer, ResourceVertexMultiset>();
				
		this.optC = NOT_DEPLOYED;
		
	}
	
	
	public void setPlacement(int iDspIndex, int vResMultisetIndex){
		
		ResourceVertexMultiset ums = allMultisets.get(vResMultisetIndex);
		dspToRes.put(iDspIndex, ums);
		
	}


	public void unsetPlacement(int iDspIndex){
		
		dspToRes.remove(iDspIndex);

	}
	
	public int getPlacement(int vDspIndex){
		return -1 ; 
	}

	public ResourceVertexMultiset getMultisetPlacement(int iDspIndex){
		return dspToRes.get(iDspIndex);
	}

	public Map<Integer, ResourceVertexMultiset> getMultisetPlacements(){
		return dspToRes;
	}
	
	public Status getStatus() {
		return status;
	}
	public void setStatus(IloCplex.Status cplexStatus) {
		
		if (IloCplex.Status.Optimal.equals(cplexStatus))
			this.status = Status.OPTIMAL;
		
		if (IloCplex.Status.Feasible.equals(cplexStatus))
			this.status = Status.FEASIBLE;
		
		if (IloCplex.Status.InfeasibleOrUnbounded.equals(cplexStatus) || 
				IloCplex.Status.Error.equals(cplexStatus))
			this.status = Status.INVALID;
		
		if (IloCplex.Status.Infeasible.equals(cplexStatus) || 
				IloCplex.Status.Error.equals(cplexStatus))
			this.status = Status.INVALID;
		
		if (IloCplex.Status.Unbounded.equals(cplexStatus) || 
				IloCplex.Status.Error.equals(cplexStatus))
			this.status = Status.INVALID;

	}
	public double getOptObjValue() {
		return optObjValue;
	}
	public void setOptObjValue(double optObjValue) {
		this.optObjValue = optObjValue;
	}
	public double getOptR() {
		return optR;
	}
	public void setOptR(double optR) {
		this.optR = optR;
	}
	public double getOptC() {
		return optC;
	}
	public void setOptC(double optC) {
		this.optC = optC;
	}
	public double getOptLogA() {
		return optLogA;
	}
	public void setOptLogA(double optLogA) {
		this.optLogA = optLogA;
	}
	public double getOptZ() {
		return optZ;
	}
	public void setOptZ(double optZ) {
		this.showZ = true;
		this.optZ = optZ;
	}
	/**
	 * Resolution time, expressed in milliseconds
	 * 
	 * @return
	 */
	public long getResolutionTime() {
		return resolutionTime;
	}
	public void setResolutionTime(long resolutionTime) {
		this.resolutionTime = resolutionTime;
	}
	public long getCompilationTime() {
		return compilationTime;
	}
	public void setCompilationTime(long compilationTime) {
		this.compilationTime = compilationTime;
	}

	@Override
	public String toString() {
		String str = "";
		
		str = "{";
		
		for(Integer iDsp : dspToRes.keySet()){
			ResourceVertexMultiset ums = dspToRes.get(iDsp);
			str += String.format("op#%d:(%d)%s;", iDsp, ums.getIndex(), ums);
		}
		str += "}";
		
		return str;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		
		if (obj == null)
			return false;
		
		OptimalMultisetSolution other = (OptimalMultisetSolution) obj;

		return dspToRes.equals(other.dspToRes);

	}

}
