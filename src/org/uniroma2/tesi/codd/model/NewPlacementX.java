package org.uniroma2.tesi.codd.model;

import ilog.concert.IloException;
import ilog.concert.IloModeler;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplexModeler;


public class NewPlacementX {
	
	protected IloModeler modeler;
	protected IloNumVar X[][];
	
	public NewPlacementX(MSoaGraph dsp, ServiceGraph res) throws IloException {
		
		/* XXX: to be fixed */
		int resSize = res.getVertices().size();
		for (ServiceVertex uRes : res.getVertices().values()){
			if (uRes.getIndex() >= resSize)
				resSize = uRes.getIndex() + 1;
		}
	
		this.X = new IloNumVar[dsp.getVertices().size()][resSize];
//		this.X = new IloNumVar[dsp.getVertices().size()][res.getVertices().size()];
		this.modeler = new IloCplexModeler();
		
		/* XXX: you can restrict x_iu here... */
		for (MSoaVertex dspOperator : dsp.getVertices().values()) {
			for (ServiceVertex resNode : res.getVertices().values()) {
				int i = dspOperator.getIndex();
				int u = resNode.getIndex();
				this.X[i][u] = this.modeler.boolVar("x[" + i + "][" + u + "]");
				
			}				
		}		
		
	}
	
	public IloNumVar get(final int i, final int u) {
		if (i > X.length - 1 || u > X[i].length - 1)
			return null;
		
		return this.X[i][u];
	}	

	@Override
	public String toString() {
		return "x(" + super.toString() + ")";
	}

}