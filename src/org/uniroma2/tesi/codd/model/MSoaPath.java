package org.uniroma2.tesi.codd.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MSoaPath {
	
	protected  ArrayList<Integer> vertexIndexSequence;
	
	public MSoaPath(ArrayList<Integer> path) {
		vertexIndexSequence = path;
	}

	public MSoaPath(Integer singleNode) {
		vertexIndexSequence = new ArrayList<Integer>();
		vertexIndexSequence.add(singleNode);
	}

	public MSoaPath() {
		this(new ArrayList<Integer>());
	}

	public void setPath(ArrayList<Integer> path){
		
		vertexIndexSequence = path;
		
	}

	public boolean add(Integer vertexIndex){
		
		return vertexIndexSequence.add(vertexIndex);
		
	}
	
	public Integer get(int index){
		
		return vertexIndexSequence.get(index);
		
	}
	
	public List<Integer> getNodesIndexes(){

		return vertexIndexSequence;
	
	}

	
	public Integer getSource(){
		
		if (vertexIndexSequence == null || vertexIndexSequence.isEmpty())
			return null;
		
		return vertexIndexSequence.get(0);
		
	}

	public boolean isSource(Integer sourceIndex){
		
		if (vertexIndexSequence == null || vertexIndexSequence.isEmpty() || sourceIndex == null)
			return false;
		
		return vertexIndexSequence.get(0).equals(sourceIndex);
		
	}
	
	public Integer getSink(){
		
		if (vertexIndexSequence == null || vertexIndexSequence.isEmpty())
			return null;
		
		return vertexIndexSequence.get(vertexIndexSequence.size() - 1);
		
	}
	
	public boolean isSink(Integer sinkIndex){
		
		if (vertexIndexSequence == null || vertexIndexSequence.isEmpty() || sinkIndex == null)
			return false;
		
		return vertexIndexSequence.get(vertexIndexSequence.size() - 1).equals(sinkIndex);
		
	}
	
	@Override
	public String toString() {
		
		String str = "(";

		Iterator<Integer> vIndex = vertexIndexSequence.iterator();
		while(vIndex.hasNext()){
			Integer n = vIndex.next();
			str += n;
			
			if (vIndex.hasNext()){ 
				str += ", "; 
			}
		}
		str += ")";

		return str;
		
	}
	
	@Override
	public int hashCode() {
		return vertexIndexSequence.hashCode();
	}
	
	@Override
	public MSoaPath clone(){
		@SuppressWarnings("unchecked")
		MSoaPath clone = new MSoaPath((ArrayList<Integer>) vertexIndexSequence.clone());

		return clone;		
	}
	@Override
	public boolean equals(Object obj) {
		
		if (obj == null)
			return false;
		
		MSoaPath other = (MSoaPath) obj;
		
		if (vertexIndexSequence.size() != other.vertexIndexSequence.size())
			return false;
		
		for (int i = 0; i < vertexIndexSequence.size(); i++){
			
			if (!vertexIndexSequence.get(i).equals(other.vertexIndexSequence.get(i)))
				return false;
			
		}

		return true;
		
	}
	
}
