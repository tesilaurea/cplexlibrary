package org.uniroma2.tesi.codd.model;

public class Pair {
	
	private int a;
	private int b;
	
	public Pair(int a, int b) {
		super();
		this.a = a;
		this.b = b;
	}

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}
	
	@Override
	public int hashCode(){
		return a+b;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (obj == null){
			return false;
		}
		Pair cobj = (Pair) obj;
		return a == cobj.a && b == cobj.b;
	}

	@Override
	public String toString() {
		return "(" + a + "," + b + ")";
	}
}
