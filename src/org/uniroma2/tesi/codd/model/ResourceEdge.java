package org.uniroma2.tesi.codd.model;

public class ResourceEdge {

	protected int from;
	protected int to;
	protected double bandwidth; 
	protected int delay; 
	protected double availability;
	protected double costPerUnitData;
	
	public ResourceEdge(int from, int to, double bandwidth, int delay,
			double availability, double costPerUnitData) {
		super();
		this.from = from;
		this.to = to;
		this.bandwidth = bandwidth;
		this.delay = delay;
		this.availability = availability;
		this.costPerUnitData = costPerUnitData;
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}

	public double getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(double bandwidth) {
		this.bandwidth = bandwidth;
	}

	public int getDelay() {
		return delay;
	}

	public void setDelay(int delay) {
		this.delay = delay;
	}

	public double getAvailability() {
		return availability;
	}

	public void setAvailability(double availability) {
		this.availability = availability;
	} 
	

	public double getCostPerUnitData() {
		return costPerUnitData;
	}

	public void setCostPerUnitData(double costPerUnitData) {
		this.costPerUnitData = costPerUnitData;
	}

	@Override
	public String toString() {
		return "[er(" + from + "," + to + "), b"+ bandwidth + ", d"+ delay + ", a"+ availability + "]";
	}
		
	@Override
	public int hashCode() {
		return from * 1000 + to;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		ResourceEdge other = (ResourceEdge) obj;
		
		return (from == other.from && to == other.to);
			
	}

	
}
