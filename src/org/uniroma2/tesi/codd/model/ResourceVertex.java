package org.uniroma2.tesi.codd.model;

public class ResourceVertex {

	protected int index;
	protected String id;
	protected int availableResources;
	protected double speedup;
	protected double availability;
	protected double instanceLaunchTime;
	protected double uploadRateToDS;
	protected double downloadRateFromDS;
	protected double uploadRateToLocalDS;
	protected double downloadRateFromLocalDS;
	protected double dataStoreRTT;

	protected boolean surrogateNode;
	protected ResourceGraph innerResourceGraph;

	public ResourceVertex(int index, String id, int availableResources, double speedup, double availability,
			double instanceLaunchTime, double uploadRateToDS, double downloadRateFromDS, double uploadRateToLocalDS,
			double downloadRateFromLocalDS, double dataStoreRTT) {
		super();
		this.id = id;
		this.index = index;
		this.availableResources = availableResources;
		this.speedup = speedup;
		this.availability = availability;

		this.surrogateNode = false;
		this.innerResourceGraph = null;

		this.downloadRateFromDS = downloadRateFromDS;
		this.uploadRateToDS = uploadRateToDS;
		this.downloadRateFromLocalDS = downloadRateFromLocalDS;
		this.uploadRateToLocalDS = uploadRateToLocalDS;
		this.instanceLaunchTime = instanceLaunchTime;
		this.dataStoreRTT = dataStoreRTT;
	}

	public ResourceVertex(int index, String id, int availableResources, double speedup, double availability) {
		this(index, id, availableResources, speedup, availability, 0.0, Double.POSITIVE_INFINITY,
				Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, 0.0);
	}

	public ResourceVertex(int index, int availableResources, double speedup, double availability) {
		this(index, Integer.toString(index), availableResources, speedup, availability);
	}

	public ResourceVertex(int index, int availableResources, double speedup, double availability,
			double instanceLaunchTime, double uploadRateToDS, double downloadRateFromDS, double uploadRateToLocalDS,
			double downloadRateFromLocalDS, double dataStoreRTT) {
		this(index, Integer.toString(index), availableResources, speedup, availability, instanceLaunchTime,
				uploadRateToDS, downloadRateFromDS, uploadRateToLocalDS, downloadRateFromLocalDS, dataStoreRTT);
	}


	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getAvailableResources() {
		return availableResources;
	}

	public void setAvailableResources(int availableResources) {
		this.availableResources = availableResources;
	}

	public double getSpeedup() {
		return speedup;
	}

	public void setSpeedup(double speedup) {
		this.speedup = speedup;
	}

	public double getAvailability() {
		return availability;
	}

	public void setAvailability(double availability) {
		this.availability = availability;
	}

	public double getInstanceLaunchTime() {
		return instanceLaunchTime;
	}

	public void setInstanceLaunchTime(double instanceLaunchTime) {
		this.instanceLaunchTime = instanceLaunchTime;
	}

	public double getUploadRateToDS() {
		return uploadRateToDS;
	}

	public void setUploadRateToDS(double uploadRateToDS) {
		this.uploadRateToDS = uploadRateToDS;
	}

	public double getDownloadRateFromDS() {
		return downloadRateFromDS;
	}

	public void setDownloadRateFromDS(double downloadRateFromDS) {
		this.downloadRateFromDS = downloadRateFromDS;
	}

	public double getUploadRateToLocalDS() {
		return uploadRateToLocalDS;
	}

	public void setUploadRateToLocalDS(double uploadRateToLocalDS) {
		this.uploadRateToLocalDS = uploadRateToLocalDS;
	}

	public double getDownloadRateFromLocalDS() {
		return downloadRateFromLocalDS;
	}

	public void setDownloadRateFromLocalDS(double downloadRateFromLocalDS) {
		this.downloadRateFromLocalDS = downloadRateFromLocalDS;
	}

	public double getDataStoreRTT() {
		return dataStoreRTT;
	}

	public void setDataStoreRTT(double dataStoreRTT) {
		this.dataStoreRTT = dataStoreRTT;
	}

	public boolean isSurrogateNode() {
		return surrogateNode;
	}

	public void setSurrogateNode(boolean surrogateNode) {
		this.surrogateNode = surrogateNode;
	}

	public ResourceGraph getInnerResourceGraph() {
		return innerResourceGraph;
	}

	public void setInnerResourceGraph(ResourceGraph innerResourceGraph) {
		this.innerResourceGraph = innerResourceGraph;
		this.surrogateNode = true;
	}

	@Override
	public String toString() {
		String ign = "";
		if (this.innerResourceGraph != null) {
			ign = ": {";
			for (ResourceVertex iv : innerResourceGraph.getVertices().values()) {
				ign += iv.getIndex() + ",";
			}
			ign += "}";
		}

		return "[vr" + index + " r" + availableResources + " s" + speedup + " a" + availability + " - " + surrogateNode
				+ ign + " ]";
	}

	@Override
	public int hashCode() {
		return index;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		ResourceVertex other = (ResourceVertex) obj;

		return index == other.index;
	}

}
