package org.uniroma2.tesi.codd.model;

import ilog.cplex.IloCplex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.uniroma2.tesi.codd.ResGraphBuilder;

public class OptimalSolution {

	/* INVALID stands for Infisible or unbounded solution */
	public enum Status { OPTIMAL, FEASIBLE, INVALID, NOT_DEFINED };
	
	protected int[] dspToRes;
	protected Status status;
	protected double optObjValue;
	protected double optR;
	protected double optLogA; 	
	protected double optZ;
	protected double optC;
	protected boolean showZ;

//	protected ArrayList<EdgeMapping> edgeMappings; 
	
	protected long resolutionTime; 
	protected long compilationTime; 
	protected long expansionTime;
	private List<Integer> usedNodes;
	private List<Pair> usedNetworkLinks;
	public static final int NOT_DEPLOYED = -1;
	
	public OptimalSolution(int numberOfDspVertices) {
	
		showZ = false;
		dspToRes = new int[numberOfDspVertices];
		
		for (int i = 0; i < dspToRes.length; i++)
			dspToRes[i] = NOT_DEPLOYED;
		
//		this.edgeMappings = new ArrayList<OptimalSolution.EdgeMapping>();
		
		this.status = Status.NOT_DEFINED;
		
		this.optObjValue = NOT_DEPLOYED;
		this.optR = NOT_DEPLOYED;
		this.optLogA = NOT_DEPLOYED;
		this.optZ = NOT_DEPLOYED;
		this.optC = NOT_DEPLOYED;

		this.resolutionTime = Long.MAX_VALUE;
		this.compilationTime = Long.MAX_VALUE;
		this.expansionTime = Long.MAX_VALUE;
		
		this.usedNodes = new ArrayList<Integer>();
		this.usedNetworkLinks = new ArrayList<Pair>();
		
	}
	
	
	public void setPlacement(int vDspIndex, int vResIndex){
		
		if (!(vDspIndex < dspToRes.length))
			throw new IndexOutOfBoundsException();
		
		dspToRes[vDspIndex] = vResIndex;
		
	}

//	public void setPlacement(int i, int j, int u, int v, double percentage){
//		
//		edgeMappings.add(new EdgeMapping(i, j, u, v, percentage));
//		
//	}
	
//	/** XXX: altamente inefficiente **/
//	public double getPlacement(int i, int j, int u, int v){
//		
//		EdgeMapping toFind = new EdgeMapping(i, j, u, v, 0.0);
//		Iterator<EdgeMapping> it = edgeMappings.iterator();
//		
//		while (it.hasNext()){
//			EdgeMapping em = it.next(); 
//			if (em.equals(toFind))
//				return em.getPercentage();
//		}
//		return 0.0;
//		
//	}

	public void unsetPlacement(int vDspIndex){
		
		if (!(vDspIndex < dspToRes.length))
			throw new IndexOutOfBoundsException();
		
		dspToRes[vDspIndex] = NOT_DEPLOYED;
		
	}
	
	public int getPlacement(int vDspIndex){
		return dspToRes[vDspIndex];
	}
	
	public Status getStatus() {
		return status;
	}
	public void setStatus(IloCplex.Status cplexStatus) {
		
		if (IloCplex.Status.Optimal.equals(cplexStatus))
			this.status = Status.OPTIMAL;
		
		if (IloCplex.Status.Feasible.equals(cplexStatus))
			this.status = Status.FEASIBLE;
		
		if (IloCplex.Status.InfeasibleOrUnbounded.equals(cplexStatus) || 
				IloCplex.Status.Error.equals(cplexStatus))
			this.status = Status.INVALID;
		
		if (IloCplex.Status.Infeasible.equals(cplexStatus) || 
				IloCplex.Status.Error.equals(cplexStatus))
			this.status = Status.INVALID;
		
		if (IloCplex.Status.Unbounded.equals(cplexStatus) || 
				IloCplex.Status.Error.equals(cplexStatus))
			this.status = Status.INVALID;
			
		
	}
	public double getOptObjValue() {
		return optObjValue;
	}
	public void setOptObjValue(double optObjValue) {
		this.optObjValue = optObjValue;
	}
	public double getOptR() {
		return optR;
	}
	public void setOptR(double optR) {
		this.optR = optR;
	}
	public double getOptLogA() {
		return optLogA;
	}
	public void setOptLogA(double optLogA) {
		this.optLogA = optLogA;
	}
	public double getOptZ() {
		return optZ;
	}
	public void setOptZ(double optZ) {
		this.showZ = true;
		this.optZ = optZ;
	}

	public void setOptC(double optC) {
		this.optC = optC;
	}

	public double getOptC() {
		return optC;
	}

	/**
	 * Resolution time, expressed in milliseconds
	 * 
	 * @return
	 */
	public long getResolutionTime() {
		return resolutionTime;
	}
	public void setResolutionTime(long resolutionTime) {
		this.resolutionTime = resolutionTime;
	}
	public long getCompilationTime() {
		return compilationTime;
	}
	public void setCompilationTime(long compilationTime) {
		this.compilationTime = compilationTime;
	}
	public long getExpansionTime() {
		return expansionTime;
	}
	public void setExpansionTime(long expansionTime) {
		this.expansionTime = expansionTime;
	}

	public void addNode(Integer vRes){
		usedNodes.add(vRes);

	}

	public void addLink(Integer uRes, Integer vRes){
		Pair link = new Pair(uRes, vRes);
		usedNetworkLinks.add(link);
	}

	public List<Integer> getUsedNodes() {
		return usedNodes;
	}
	public List<Pair> getUsedNetworkLinks() {
		return usedNetworkLinks;
	}

	@Override
	public String toString() {
		String str = "";
		
//		str += "optPlacement["+ optObjValue + ", Status=" + status + ", R="+ optR + ", logA=" + optLogA;
//		if (showZ)
//			str += ", Z=" + optZ;
//		str += "]";
		
		str = "{";
		for (int i = 0; i < dspToRes.length; i++){
			str += "" + dspToRes[i];
			
			if (i < (dspToRes.length - 1)){
				str += "; ";
			}
		}
		str += "}";
		
//		/* Append edge mapping */
//		str += ", {";
//		for (EdgeMapping e : edgeMappings){
//			if (e.getPercentage() > 0.0)
//				str += e.toString() + "; ";
//		}
//		str += "}";
//		/*/Append edge mapping */

		
		return str;
	}
	
	public String toUidString(ResGraphBuilder resGraphBuilder) {
		Map<Integer, ServiceVertex> vertices = 
				resGraphBuilder.getServiceGraph().getVertices();
		String str = "{";
		for (int i = 0; i < dspToRes.length; i++){
			for(ServiceVertex s:vertices.values()){
				if(s.getIndex() == dspToRes[i]){
					str += "" + s.getId();
				}
				
			}
			if (i < (dspToRes.length - 1)){
				str += "; ";
			}
		}
		str += "}";
		return str;
	}
	
	public Map<Integer, ServiceVertex> getSolutionVertex(
			ResGraphBuilder resGraphBuilder){
		
			Map<Integer, ServiceVertex> vertices = 
					resGraphBuilder.getServiceGraph().getVertices();
			Map<Integer, ServiceVertex> solution = new HashMap<Integer, ServiceVertex>();
				
			for (int i = 0; i < dspToRes.length; i++){
				/*str += "" + vertices.getOrDefault(dspToRes[i], null).getIp();
				
				if (i < (dspToRes.length - 1)){
					str += "; ";
				}*/
				/*for(ServiceVertex s:vertices.values()){
					if(Integer.parseInt(s.getId()) == dspToRes[i]){
						solution.put(Integer.parseInt(s.getId()), s);
					}
					
				}*/
				for(ServiceVertex s:vertices.values()){
					if(s.getIndex() == dspToRes[i]){
						solution.put(s.getIndex(), s);//Integer.parseInt(s.getId()), s);
					}
					
				}
			}
			
		
		return solution;
		
	}
	
	public Map<Pair, ServiceEdge> getSolutionEdge(
			ResGraphBuilder resGraphBuilder){
			Map<Integer, ServiceVertex> vertices = 
				resGraphBuilder.getServiceGraph().getVertices();
			Map<Pair, ServiceEdge> edges = 
					resGraphBuilder.getServiceGraph().getEdges();
			Map<Pair, ServiceEdge> solution = new HashMap<Pair, ServiceEdge>();
			
			for (int i = 0; i < dspToRes.length; i++){
				for (int j = 0; j < dspToRes.length; j++){
					
					for(ServiceEdge e : edges.values()){
						if(e.from == dspToRes[i] && e.to == dspToRes[j]){
							solution.put(new Pair(dspToRes[i], dspToRes[j]), 
									e);
							break;
						}
					}
					/*if(null != edges.getOrDefault(
							new Pair(dspToRes[i], dspToRes[j]), null))
					{		
						solution.put(new Pair(dspToRes[i], dspToRes[j]),
								edges.getOrDefault(
										new Pair(dspToRes[i], dspToRes[j]), null));
						
					}*/
					
				}
			}
		return solution;
		
	}
	
	public String getSolution(ResGraphBuilder resGraphBuilder) {
		String str = "";
		
//		str += "optPlacement["+ optObjValue + ", Status=" + status + ", R="+ optR + ", logA=" + optLogA;
//		if (showZ)
//			str += ", Z=" + optZ;
//		str += "]";
		Map<Integer, ServiceVertex> vertices = 
				resGraphBuilder.getServiceGraph().getVertices();
		
		str = "{";
		for (int i = 0; i < dspToRes.length; i++){
			/*str += "" + vertices.getOrDefault(dspToRes[i], null).getIp();
			
			if (i < (dspToRes.length - 1)){
				str += "; ";
			}*/
			for(ServiceVertex s:vertices.values()){
				if(s.getIndex() == dspToRes[i]){
					str += "" + s.getIp();
					if (i < (dspToRes.length - 1))
						str += "; ";
				}
				
			}
		}
		str += " ";
		str += "}";
		
//		/* Append edge mapping */
//		str += ", {";
//		for (EdgeMapping e : edgeMappings){
//			if (e.getPercentage() > 0.0)
//				str += e.toString() + "; ";
//		}
//		str += "}";
//		/*/Append edge mapping */
		return str;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (obj == null)
			return false;
		
		OptimalSolution other = (OptimalSolution) obj;

		if (this.dspToRes.length != other.dspToRes.length)
			return false;
		
		for (int i = 0; i < dspToRes.length; i++){
			if (dspToRes[i] != other.dspToRes[i])
				return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		return dspToRes.hashCode();
	}

}
