package org.uniroma2.tesi.codd.model;

public class MSoaEdge {

	protected int from;
	protected int to;

	public MSoaEdge(int from, int to) {
		super();
		this.from = from;
		this.to = to;
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}

	
		
	@Override
	public String toString() {
		return "MSoaEdge [from=" + from + ", to=" + to + "]";
	}

	@Override
	public int hashCode() {
		return from * 1000 + to;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		MSoaEdge other = (MSoaEdge) obj;
		
		return (from == other.from && to == other.to);
			
	}

}
