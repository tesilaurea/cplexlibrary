package org.uniroma2.tesi.codd.metricsprovider;

import org.uniroma2.tesi.codd.metricsprovider.estimator.ExecutionTimeEstimator;
import org.uniroma2.tesi.codd.metricsprovider.estimator.TaskPairBandwidthEstimator;

//non mi serve il bandwith
public class ApplicationMetricsProvider {

	private ExecutionTimeEstimator execTime;
	private TaskPairBandwidthEstimator bandwidth;

	public ApplicationMetricsProvider() {

		this.execTime = new ExecutionTimeEstimator();
		this.bandwidth = new TaskPairBandwidthEstimator();

	}

	public double getExecutionTime(int vDsp) {
		return execTime.getTaskExecutionTime(vDsp);
	}

	public double getDataRate(int fromVDsp, int toVDsp) {
		return bandwidth.getBandwidth(fromVDsp, toVDsp);
	}

	public int getRequiredResources(int vDsp) {
		return 1;
	}

	public double getCost(int iDsp) {
		return 1.0;
	}

	public double getServiceRate(int iDsp) {
		return 1.0;
	}

	public int getAvgNumBytesPerTuple(int fromVDsp, int toVDsp) {
		return 1;
	}

	public double getInternalStateImageSize(int vDsp) {
		return 1.0;
	}

	public double getCodeImageSize(int vDsp) {
		return 1.0;
	}

	public double getReconfigurationSyncTime() {
		return 1.0;
	}

}
