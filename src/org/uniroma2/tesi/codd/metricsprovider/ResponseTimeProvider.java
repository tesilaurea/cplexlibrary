package org.uniroma2.tesi.codd.metricsprovider;

public interface ResponseTimeProvider {

	double getResponseTime(double lambda, int replicas);
}
