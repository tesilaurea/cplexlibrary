package org.uniroma2.tesi.codd.metricsprovider;

public class ResourceMetricsProvider {

	public ResourceMetricsProvider() {
	}

	/**
	 * Get Latency between two resource vertices
	 * 
	 * @param fromVresId
	 * @param toVresId
	 * @return
	 */
	public double getLatency(int fromVresId, int toVresId) {

		return 1.0;

	}

	/**
	 * Get Latency between two resource vertices
	 * 
	 * @param fromVresId
	 * @param toVresId
	 * @return
	 */
	public double getSerLatency(String fromNode, String toNode) {

		return 1.0;

	}
	/**
	 * Not modeled
	 * 
	 * @param fromVresId
	 * @param toVresId
	 * @return
	 */
	public double getAvailability(int fromVresId, int toVresId) {

		return 1.0;

	}

	/**
	 * Not modeled
	 * 
	 * @param fromVresId
	 * @param toVresId
	 * @return
	 */
	public double getBandwidth(int fromVresId, int toVresId) {

		if (fromVresId == toVresId)
			return Double.MAX_VALUE;

		return Double.MAX_VALUE;

	}

	/**
	 * Return availability of node with id: nodeId
	 * 
	 * UNSTABLE
	 * 
	 * @param vResId
	 * @return node availability between 0 and 1
	 */
	public double getAvailability(int vResId) {

		return 1.0;

	}

	/**
	 * Return computational speedup of node with id: nodeId
	 * 
	 * UNSTABLE
	 * 
	 * @param vResId
	 * @return node speedup > 1
	 */
	public double getSpeedup(int vResId) {
		return 1.0;
	}

	public int getAvailableResources(int vResId) {

		return 1;

	}

	public double getCostPerUnitData(int uRes, int vRes) {
		return 1;
	}

	public double getUploadRateToDS(int vRes) {
		return 1.0;
	}

	public double getDownloadRateFromDS(int vRes) {
		return 1.0;
	}

	public double getUploadRateToLocalDS(int vRes) {
		return 1.0;
	}

	public double getDownloadRateFromLocalDS(int vRes) {
		return 1.0;
	}

	public double getInstanceLaunchTime(int vRes) {
		return 1.0;
	}

	public double getDataStoreRTT (int vRes) {
		return 0.0;
	}
}
