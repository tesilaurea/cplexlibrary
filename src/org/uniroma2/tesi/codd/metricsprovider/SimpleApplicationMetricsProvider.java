package org.uniroma2.tesi.codd.metricsprovider;

import java.util.Random;

import org.uniroma2.tesi.codd.MSoaGraphBuilder;
import org.uniroma2.tesi.codd.MSoaGraphBuilder.TYPE;
import org.uniroma2.tesi.codd.ODDParameters;
// Per me questo coso non mi serve
public class SimpleApplicationMetricsProvider extends ApplicationMetricsProvider {
	
	private MSoaGraphBuilder.TYPE type;
	private int sinkId;
	private double execTimeMin;
	private double execTimeMean; 
	private double execTimeStdDev;
	
	private double lambdaMin;
	private double lambdaMean; 
	private double lambdaStdDev;
	private double lambdaSheddingFactor;
	
	private double avgBytePerTupleMin;
	private double avgBytePerTupleMean;
	private double avgBytePerTupleStdDev;
	
	private double requiredResourcesMin;
	private double requiredResourcesMean; 
	private double requiredResourcesStdDev;

	private double costPerResource;
	private double serviceRate;
	
	protected Random rnd;
	private final long seed = 201512021054l;
	
	private enum MT{ RESP_TIME, DATA_RATE, REQ_RESOURCES, BYTES_PER_TUPLE};
	
	/**
	 * Get application metrics. 
	 * A gaussian distribution is used; if a constant value is required, use mean = stdDev = 0
	 * and set the desired value to the 'min' parameter. 
	 * 
	 * @param type
	 * @param nDsp
	 * @param execTimeMin
	 * @param execTimeMean
	 * @param execTimeStdDev
	 * @param lambdaMin
	 * @param lambdaMean
	 * @param lambdaStdDev
	 * @param lambdaSheddingFactor
	 * @param requiredResourcesMin
	 * @param requiredResourcesMean
	 * @param requiredResourcesStdDev
	 */
	public SimpleApplicationMetricsProvider(TYPE type, int nDsp, 
			ODDParameters params) {
		super();
		this.type = type;
		this.sinkId = nDsp - 1;
		this.execTimeMin = params.getRespTimeMin();
		this.execTimeMean = params.getRespTimeMean();
		this.execTimeStdDev = params.getRespTimeStdDev();
		this.lambdaMin = params.getLambdaMin();
		this.lambdaMean = params.getLambdaMean();
		this.lambdaStdDev = params.getRespTimeStdDev();
		this.lambdaSheddingFactor = Math.min(params.getLambdaSheddingFactor(), 1.0);
		this.requiredResourcesMin = params.getReqResourcesMin();
		this.requiredResourcesMean = params.getReqResourcesMean();
		this.requiredResourcesStdDev = params.getReqResourcesStdDev();
		this.costPerResource = params.getCostPerResource();
		this.avgBytePerTupleMin 	= params.getAvgBytePerTupleMin();
		this.avgBytePerTupleMean 	= params.getAvgBytePerTupleMean();
		this.avgBytePerTupleStdDev	= params.getAvgBytePerTupleStdDev();
		this.serviceRate = params.getServiceRate();
		
		this.rnd = new Random(seed);
	}


	@Override
	public double getExecutionTime(int vDsp){

		return value(rnd.nextGaussian(), MT.RESP_TIME);
		
	}
	
	@Override
	public double getDataRate(int fromVDsp, int toVDsp){

		if (MSoaGraphBuilder.TYPE.SEQUENTIAL.equals(type)){		
			
			return Math.max(0.0, value(rnd.nextGaussian(), MT.DATA_RATE) * Math.pow((1 - lambdaSheddingFactor), fromVDsp));
			
		} else {
			// DspGraphBuilder.TYPE.FAT
			if (sinkId != toVDsp){
				return  value(rnd.nextGaussian(), MT.DATA_RATE);
			} else {
				 return Math.max(0.0, value(rnd.nextGaussian(), MT.DATA_RATE) * (1 - lambdaSheddingFactor));
			}
		}
	}

	@Override
	public int getRequiredResources(int vDsp){
		
		int reqi = (int) Math.floor(value(rnd.nextGaussian(), MT.REQ_RESOURCES));

		// fat component
//		if (vDsp % 4 == 0 && vDsp != 0)
//			reqi = reqi * 2;

		return reqi;
		
	}
	
	@Override
	public double getCost(int iDsp) {
		
		return costPerResource;
		
	}
	
	@Override
	public int getAvgNumBytesPerTuple(int iDsp, int jDsp) {
		
		return (int) Math.floor(value(rnd.nextGaussian(), MT.BYTES_PER_TUPLE));
	
	}
	
	@Override
	public double getServiceRate(int iDsp) {
		
		// fat component
//		if (iDsp % 4 == 0 && iDsp != 0)
//			return serviceRate / 5;

		return serviceRate;
	}
	
	private double value(double gaussian, MT mt){

		if (MT.REQ_RESOURCES.equals(mt)){
			
			if (requiredResourcesMean == 0.0 && requiredResourcesStdDev == 0.0)
				return requiredResourcesMin;
			return Math.max(requiredResourcesMin, gaussian * requiredResourcesStdDev + requiredResourcesMean);
			
		} else if (MT.RESP_TIME.equals(mt)){
			
			if (execTimeMean == 0.0 && execTimeStdDev == 0.0)
				return execTimeMin;
			return Math.max(execTimeMin, gaussian * execTimeStdDev + execTimeMean);
			
		} else if (MT.DATA_RATE.equals(mt)){

			if (lambdaMean == 0.0 && lambdaStdDev == 0.0)
				return lambdaMin;
			return Math.max(lambdaMin, gaussian * lambdaStdDev + lambdaMean);
			
		} else if (MT.BYTES_PER_TUPLE.equals(mt)){

			if (avgBytePerTupleMean == 0.0 && avgBytePerTupleStdDev == 0.0)
				return avgBytePerTupleMin;
			return Math.max(avgBytePerTupleMin, gaussian * avgBytePerTupleStdDev + avgBytePerTupleMean);
			
		} else {

			return 1.0; 
			
		}

	}
}
