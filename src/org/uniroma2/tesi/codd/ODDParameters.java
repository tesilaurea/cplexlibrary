package org.uniroma2.tesi.codd;


import java.lang.reflect.Field;

import org.uniroma2.tesi.codd.placement.ODDModel;

//cos' è sta roba?  Cosa sono z e c???
public class ODDParameters {

	protected static final double DEFAULT_RMAX 	= 2810.0;
	protected static final double DEFAULT_RMIN 	= 1230.0;
//	protected static final double DEFAULT_AMAX 	= 0.988;
//	protected static final double DEFAULT_AMIN 	= 0.850;
	protected static final double DEFAULT_AMAX 	= 0.999999;
	protected static final double DEFAULT_AMIN 	= 0.005;
//	protected static final double DEFAULT_ZMAX 	= 40.0;
//	protected static final double DEFAULT_ZMIN 	= 8.0;
	protected static final double DEFAULT_ZMAX 	= 1.0;
	protected static final double DEFAULT_ZMIN 	= 0.0;
	protected static final double DEFAULT_CMAX 	= 25.0;
	protected static final double DEFAULT_CMIN 	= 11.0;
	
	/* ******* PARAMETERS ********** */
		
	/* Application Metrics Provider */
	protected double respTimeMin		= 3.0;
	protected double respTimeMean	= 0.0; 
	protected double respTimeStdDev 	= 0.0;

	protected double avgBytePerTupleMin 		= 1500.0; // this is byte/ms (i.e., KB/s)
	protected double avgBytePerTupleMean		= 0.0; 
	protected double avgBytePerTupleStdDev	= 0.0;
	
	protected double lambdaMin	= 0.014 / 5.0; // 99.978; // 10.0; // 99.978; // 100.0;	
	protected double lambdaMean	= 0.0; 
	protected double lambdaStdDev	= 0.0;
	protected double lambdaSheddingFactor = 0.05; // 0.05; // 0.00005; // 0.15; // 0.0005;
	
	protected double reqResourcesMin		= 1.0;
	protected double reqResourcesMean	= 0.0; 
	protected double reqResourcesStdDev	= 0.0;

	protected double costPerResource 	= 1.0;

	
	/* Resource Metrics Provider */
	protected double linkDelayMin	= 1.0;;
	protected double linkDelayMean	= 22.0;
	protected double linkDelayStdDev = 5.0;

	protected double linkAvailMin	= 1;
	protected double linkAvailMean 	= 0.0;
	protected double linkAvailStdDev = 0.0;

	protected double linkBandwidthMin	= Double.MAX_VALUE;
	protected double linkBandwidthMean  = 0.0;
	protected double linkBandwidthStdDev = 0.0;

	protected double linkCostPerUnitData = 0.02;

	protected double nodeAvailResourcesMin	= 5.0;
	protected double nodeAvailResourcesMean	= 0.0;
	protected double nodeAvailResourcesStdDev	= 0.0;

	protected double nodeSpeedupMin	= 1.0;
	protected double nodeSpeedupMean	= 0.0;
	protected double nodeSpeedupStdDev	= 0.0;

	protected double nodeAvailabilityMin		= .97;		// uniform distribution
	protected double nodeAvailabilityMean	= .9999999; // used as max 
	protected double nodeAvailabilityStdDev	= .03;
	
	protected double serviceRate = 0.02; // 100.0; // 15.0; // 100.0;
	
	protected double Rmax;
	protected double Rmin;
	protected double Cmax;
	protected double Cmin;
	protected double Amax;
	protected double Amin;
	protected double Zmax;
	protected double Zmin;

	/* ODD Weights */
	protected double weightAvailability = 0;
	protected double weightRespTime 	= 1;
	protected double weightCost			= 0;
	protected double weightNetMetric 	= 0;
	
	protected boolean useTimeLimit;
	protected double timeLimit; 
	protected boolean defineOptimalGap;
	protected double gap;
	
	protected ODDModel.MODE mode;
	
	protected boolean odrpUseMM1;
	
	public ODDParameters(ODDModel.MODE mode){

		this.mode = mode;
		this.timeLimit = Double.MAX_VALUE;
		this.gap = 0.0;
		this.useTimeLimit = false;
		this.defineOptimalGap = false;
		
		this.odrpUseMM1 = true;
		
		setDefaults();
		
	}
	
	private void setDefaults(){

		Rmax = DEFAULT_RMAX;
		Rmin = DEFAULT_RMIN;
		Cmax = DEFAULT_CMAX;
		Cmin = DEFAULT_CMIN;
		Amax = DEFAULT_AMAX;
		Amin = DEFAULT_AMIN;
		Zmax = DEFAULT_ZMAX;
		Zmin = DEFAULT_ZMIN;

	}
		
	
	/*
	 *  Normalization factors 
	 */
	public double getRmax() {
		return Rmax;
	}
	public void setRmax(double rmax) {
		Rmax = rmax;
	}
	public double getRmin() {
		return Rmin;
	}
	public void setRmin(double rmin) {
		Rmin = rmin;
	}
	public double getAmax() {
		return Amax;
	}
	public void setAmax(double logAmax) {
		Amax = logAmax;
	}
	public double getAmin() {
		return Amin;
	}
	public void setAmin(double logAmin) {
		Amin = logAmin;
	}
	public double getZmax() {
		return Zmax;
	}
	public void setZmax(double zmax) {
		Zmax = zmax;
	}
	public double getZmin() {
		return Zmin;
	}
	public void setZmin(double zmin) {
		Zmin = zmin;
	}
	public double getCmax() {
		return Cmax;
	}
	public void setCmax(double cmax) {
		Cmax = cmax;
	}
	public double getCmin() {
		return Cmin;
	}
	public void setCmin(double cmin) {
		Cmin = cmin;
	}

	
	/* 
	 * Objective Function weights 
	 */
	public double getWeightRespTime() {
		return weightRespTime;
	}
	public void setWeightRespTime(double wR) {
		this.weightRespTime = wR;
	}
	
	public double getWeightAvailability() {
		return weightAvailability;
	}
	public void setWeightAvailability(double wA) {
		this.weightAvailability = wA;
	}
	
	public double getWeightNetMetric() {
		return weightNetMetric;
	}
	public void setWeightNetMetric(double wZ) {
		this.weightNetMetric = wZ;
	}
	public double getWeightCost() {
		return weightCost;
	}
	public void setWeightCost(double wC) {
		this.weightCost = wC;
	}
	
	
	/* 
	 * Application Metrics Provider 
	 */
	public double getRespTimeMin() {
		return respTimeMin;
	}
	public void setRespTimeMin(double respTimeMin) {
		this.respTimeMin = respTimeMin;
	}
	public double getRespTimeMean() {
		return respTimeMean;
	}
	public void setRespTimeMean(double respTimeMean) {
		this.respTimeMean = respTimeMean;
	}
	public double getRespTimeStdDev() {
		return respTimeStdDev;
	}
	public void setRespTimeStdDev(double respTimeStdDev) {
		this.respTimeStdDev = respTimeStdDev;
	}
	public double getAvgBytePerTupleMin() {
		return avgBytePerTupleMin;
	}
	public void setAvgBytePerTupleMin(double avgBytePerTupleMin) {
		this.avgBytePerTupleMin = avgBytePerTupleMin;
	}
	public double getAvgBytePerTupleMean() {
		return avgBytePerTupleMean;
	}
	public void setAvgBytePerTupleMean(double avgBytePerTupleMean) {
		this.avgBytePerTupleMean = avgBytePerTupleMean;
	}
	public double getAvgBytePerTupleStdDev() {
		return avgBytePerTupleStdDev;
	}
	public void setAvgBytePerTupleStdDev(double avgBytePerTupleStdDev) {
		this.avgBytePerTupleStdDev = avgBytePerTupleStdDev;
	}
	public double getLambdaMin() {
		return lambdaMin;
	}
	public void setLambdaMin(double lambdaMin) {
		this.lambdaMin = lambdaMin;
	}
	public double getLambdaMean() {
		return lambdaMean;
	}
	public void setLambdaMean(double lambdaMean) {
		this.lambdaMean = lambdaMean;
	}
	public double getLambdaStdDev() {
		return lambdaStdDev;
	}
	public void setLambdaStdDev(double lambdaStdDev) {
		this.lambdaStdDev = lambdaStdDev;
	}
	public double getLambdaSheddingFactor() {
		return lambdaSheddingFactor;
	}
	public void setLambdaSheddingFactor(double lambdaSheddingFactor) {
		this.lambdaSheddingFactor = lambdaSheddingFactor;
	}
	public double getReqResourcesMin() {
		return reqResourcesMin;
	}
	public void setReqResourcesMin(double reqResourcesMin) {
		this.reqResourcesMin = reqResourcesMin;
	}
	public double getReqResourcesMean() {
		return reqResourcesMean;
	}
	public void setReqResourcesMean(double reqResourcesMean) {
		this.reqResourcesMean = reqResourcesMean;
	}
	public double getReqResourcesStdDev() {
		return reqResourcesStdDev;
	}
	public void setReqResourcesStdDev(double reqResourcesStdDev) {
		this.reqResourcesStdDev = reqResourcesStdDev;
	}
	public double getCostPerResource() {
		return costPerResource;
	}
	public void setCostPerResource(double costPerResource) {
		this.costPerResource = costPerResource;
	}

	
	/* 
	 * Resource Metrics Provider 
	 */
	public double getLinkDelayMin() {
		return linkDelayMin;
	}
	public void setLinkDelayMin(double linkDelayMin) {
		this.linkDelayMin = linkDelayMin;
	}
	public double getLinkDelayMean() {
		return linkDelayMean;
	}
	public void setLinkDelayMean(double linkDelayMean) {
		this.linkDelayMean = linkDelayMean;
	}
	public double getLinkDelayStdDev() {
		return linkDelayStdDev;
	}
	public void setLinkDelayStdDev(double linkDelayStdDev) {
		this.linkDelayStdDev = linkDelayStdDev;
	}
	public double getLinkAvailMin() {
		return linkAvailMin;
	}
	public void setLinkAvailMin(double linkAvailMin) {
		this.linkAvailMin = linkAvailMin;
	}
	public double getLinkAvailMean() {
		return linkAvailMean;
	}
	public void setLinkAvailMean(double linkAvailMean) {
		this.linkAvailMean = linkAvailMean;
	}
	public double getLinkAvailStdDev() {
		return linkAvailStdDev;
	}
	public void setLinkAvailStdDev(double linkAvailStdDev) {
		this.linkAvailStdDev = linkAvailStdDev;
	}
	public double getLinkBandwidthMin() {
		return linkBandwidthMin;
	}
	public void setLinkBandwidthMin(double linkBandwidthMin) {
		this.linkBandwidthMin = linkBandwidthMin;
	}
	public double getLinkBandwidthMean() {
		return linkBandwidthMean;
	}
	public void setLinkBandwidthMean(double linkBandwidthMean) {
		this.linkBandwidthMean = linkBandwidthMean;
	}
	public double getLinkBandwidthStdDev() {
		return linkBandwidthStdDev;
	}
	public void setLinkBandwidthStdDev(double linkBandwidthStdDev) {
		this.linkBandwidthStdDev = linkBandwidthStdDev;
	}
	public double getLinkCostPerUnitData() {
		return linkCostPerUnitData;
	}
	public void setLinkCostPerUnitData(double linkCostPerUnitData) {
		this.linkCostPerUnitData = linkCostPerUnitData;
	}
	public double getNodeAvailResourcesMin() {
		return nodeAvailResourcesMin;
	}
	public void setNodeAvailResourcesMin(double nodeAvailResourcesMin) {
		this.nodeAvailResourcesMin = nodeAvailResourcesMin;
	}
	public double getNodeAvailResourcesMean() {
		return nodeAvailResourcesMean;
	}
	public void setNodeAvailResourcesMean(double nodeAvailResourcesMean) {
		this.nodeAvailResourcesMean = nodeAvailResourcesMean;
	}
	public double getNodeAvailResourcesStdDev() {
		return nodeAvailResourcesStdDev;
	}
	public void setNodeAvailResourcesStdDev(double nodeAvailResourcesStdDev) {
		this.nodeAvailResourcesStdDev = nodeAvailResourcesStdDev;
	}
	public double getNodeSpeedupMin() {
		return nodeSpeedupMin;
	}
	public void setNodeSpeedupMin(double nodeSpeedupMin) {
		this.nodeSpeedupMin = nodeSpeedupMin;
	}
	public double getNodeSpeedupMean() {
		return nodeSpeedupMean;
	}
	public void setNodeSpeedupMean(double nodeSpeedupMean) {
		this.nodeSpeedupMean = nodeSpeedupMean;
	}
	public double getNodeSpeedupStdDev() {
		return nodeSpeedupStdDev;
	}
	public void setNodeSpeedupStdDev(double nodeSpeedupStdDev) {
		this.nodeSpeedupStdDev = nodeSpeedupStdDev;
	}
	public double getNodeAvailabilityMin() {
		return nodeAvailabilityMin;
	}
	public void setNodeAvailabilityMin(double nodeAvailabilityMin) {
		this.nodeAvailabilityMin = nodeAvailabilityMin;
	}
	public double getNodeAvailabilityMean() {
		return nodeAvailabilityMean;
	}
	public void setNodeAvailabilityMean(double nodeAvailabilityMean) {
		this.nodeAvailabilityMean = nodeAvailabilityMean;
	}
	public double getNodeAvailabilityStdDev() {
		return nodeAvailabilityStdDev;
	}
	public void setNodeAvailabilityStdDev(double nodeAvailabilityStdDev) {
		this.nodeAvailabilityStdDev = nodeAvailabilityStdDev;
	}
	public double getServiceRate() {
		return serviceRate;
	}
	public void setServiceRate(double serviceRate) {
		this.serviceRate = serviceRate;
	}
	public void setDefineOptimalGap(boolean defineOptimalGap) {
		this.defineOptimalGap = defineOptimalGap;
	}

	
	/* 
	 * Other parametes 
	 */
	public ODDModel.MODE getMode() {
		return mode;
	}
	public void setMode(ODDModel.MODE mode) {
		this.mode = mode;
	}

	public double getTimeLimit() {
		return timeLimit;
	}

	public boolean isOdrpUseMM1(){
		return odrpUseMM1;
	}
	
	public void setOdrpMM1(boolean odrpUseMM1){
		this.odrpUseMM1 = odrpUseMM1;
	}
	
	/**
	 * Define a timeout to the execution of cplex.solve().
	 * 
	 * @param timeLimit expressed in seconds.
	 */
	public void setTimeLimit(double timeLimit) {
		useTimeLimit = true;
		this.timeLimit = timeLimit;
	}
	public double getGap() {
		return gap;
	}
	/**
	 * Define a tolerance on the gap between the best 
	 * integer objective and the best node remining (branch and cut)
	 * 
	 * For further details, see 
	 * http://www-eio.upc.es/lceio/manuals/cplex-11/html/refparameterscplex/refparameterscplex40.html
	 * 
	 * @param gap
	 */
	public void setGap(double gap) {
		defineOptimalGap = true;
		this.gap = gap;
	}

	public boolean isUseTimeLimit() {
		return useTimeLimit;
	}

	public boolean isDefineOptimalGap() {
		return defineOptimalGap;
	}


	public void setWeights(double availability, double responseTime, double cost, double netMetric)
	{
		if (availability + responseTime + cost + netMetric > 1.0)
			throw new RuntimeException("Sum of weights exceeds 1!");

		this.weightAvailability = availability;
		this.weightRespTime = responseTime;
		this.weightCost = cost;
		this.weightNetMetric = netMetric;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		
		for (Field field : this.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			String name = field.getName();
			Object value;
			try {
				value = field.get(this);
				sb.append(String.format("%s=%s%n", name, value));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				continue;
			}
		}
		
		return sb.toString();
	}

}
