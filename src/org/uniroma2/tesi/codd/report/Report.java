package org.uniroma2.tesi.codd.report;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.uniroma2.tesi.codd.MSoaGraphBuilder;
import org.uniroma2.tesi.codd.ODDParameters;
import org.uniroma2.tesi.codd.ResGraphBuilder;
import org.uniroma2.tesi.codd.metricsprovider.ApplicationMetricsProvider;
import org.uniroma2.tesi.codd.metricsprovider.ResourceMetricsProvider;
import org.uniroma2.tesi.codd.model.OptimalMultisetSolution;
import org.uniroma2.tesi.codd.model.OptimalSolution;
import org.uniroma2.tesi.codd.placement.ODDModel;

public class Report {

	protected String filename;
	protected String experimentId;
	
	public Report(String experimentId, String filename) {
	
		this.filename = filename;
		this.experimentId = experimentId;
		
	}
	
	public void write(String idSeries, int run, ODDModel model, 
			MSoaGraphBuilder gbuilder, ApplicationMetricsProvider amp, 
			ResGraphBuilder rbuilder, ResourceMetricsProvider rmp,
			double restrictionOnVres,
			ODDParameters params, OptimalSolution solution) throws ReportException, IOException{
		write(idSeries, run, model, gbuilder, amp, rbuilder, rmp, restrictionOnVres, -1, -1, params, solution);
	}
	public void write(String idSeries, int run, ODDModel model, 
			MSoaGraphBuilder gbuilder, ApplicationMetricsProvider amp, 
			ResGraphBuilder rbuilder, ResourceMetricsProvider rmp,
			double restrictionOnVres, int srcRes, int snkRes,
			ODDParameters params, OptimalSolution solution) throws ReportException, IOException{

		File f = new File(filename);

		boolean writeHeadings = true;
				
		if (!f.canWrite()){
			f.createNewFile();
			writeHeadings = true;
		}
		
		BufferedWriter buffer = null;
			
		try {
			buffer = new BufferedWriter(new FileWriter(f, true));
			
			// XXX: fallback for the C parameter, which has been introduced only in ReplODP and in OptimalMultisetSolution
			String costInformation = "";
			if (solution instanceof OptimalMultisetSolution){
				costInformation = ((OptimalMultisetSolution) solution).getOptC() + ", " + // C
						(params.getWeightCost() * (params.getCmax() - ((OptimalMultisetSolution) solution).getOptC()) / (params.getCmax() - params.getCmin())) + ", "; // term_C
			} else {
				costInformation = -1 + ", " + // C
								   0 + ", ";  // term_C
			}
			
			/*
			 * id; model; res; #res; dsp; type; #vdsp; #edsp; #pinned; #%request; cplex; gap; timeout; compilationTime; resolutionTime; opt; solution; obj; r; a; z; x
			 */
			if (writeHeadings){
				String head = "#report; idSerie; id; #run; model; "
						+ "res; #res; #Cu; "
						+ "dsp; type; #vdsp; #edsp; #pinned; "
						+ "#%request; %restrictionOnVres; "
						+ "pinned; srcRes; snkRes; "
						+ "cplex; gap; timeout; compilationTime; resolutionTime; "
						+ "opt; solution; obj; R; term_R; C; term_C; A; term_A; Z; term_Z; "
						+ "; "
						+ "x; \n";
				System.out.println(head);
				buffer.write(head);
			}
			// String str =  String.format("#report#%s;%s;res;%d;dsp;%s;%d;%d;%d;%f;cplex;%f;%f;%d;%d;opt;%s;%f;%f;%f;%f;%s;\n",
			String str = "report, " + 
					idSeries + ", " + 
					this.experimentId + ", " + 
					run + ", " + 
					(model == null ? "heuristic" : model.type()) + ", " +
				"res, " +
					rbuilder.getServiceGraph().getVertices().size() + ", " +
					rmp.getAvailableResources(0) + ", " +
				"dsp, " +
					gbuilder.getType() + ", " +
					gbuilder.getGraph().getVertices().size() + ", " +
					gbuilder.getGraph().getEdges().size() + ", " +
					2 + ", " +
					((double)gbuilder.getGraph().getVertices().size() / 
							(double) rbuilder.getServiceGraph().getVertices().size()) 
					+ ", " +
					restrictionOnVres + ", " +
				"pinned, " + 
					srcRes + ", " +
					snkRes + ", " + 
				"cplex, " +
					params.getGap() + ", " +
					params.getTimeLimit() + ", " +
					solution.getCompilationTime() + ", " +
					solution.getResolutionTime() + ", " +
				"opt, " +
					solution.getStatus() + ", " +
					solution.getOptObjValue() + ", " +
					solution.getOptR() + ", " + // R
					(params.getWeightRespTime() * (params.getRmax() - solution.getOptR()) / (params.getRmax() - params.getRmin())) + ", " + // term_R
					costInformation +
					Math.exp(solution.getOptLogA()) + ", " + // A
					(solution.getOptLogA() - Math.log(params.getAmin())) * params.getWeightAvailability() / (Math.log(params.getAmax()) - Math.log(params.getAmin())) + ", " + // term_A
					solution.getOptZ() + ", " + // Z
					(params.getWeightNetMetric() * (params.getZmax() -  solution.getOptZ()) / (params.getZmax() - params.getZmin())) + ", " + // term_Z
					solution.getOptC() + ", " + // C
					(params.getWeightCost() * (params.getCmax() - solution.getOptC()) / (params.getCmax() - params.getCmin())) + ", " + // term_C
				"," +
				solution.toString() + ", \n";
			
			buffer.write(str);
			System.out.println(str);
			
			System.out.println(solution.getSolution(rbuilder));
			buffer.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if (buffer!= null)
				buffer.close();
		}
	}
}
