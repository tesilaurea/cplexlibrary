package org.uniroma2.tesi.codd.utils;
import java.util.Iterator;

public class Multiset implements Iterable<Integer>{

	private int[] multiset;
    private int size;

    public Multiset(int[] multiset) {
        this.multiset = multiset.clone();
        this.size = multiset.length;
    }
    
    public int size(){
    	return size;
    }
    
    public int[] toArray(){
    	return multiset.clone();
    }
    
	@Override
	public Iterator<Integer> iterator() {
		return new Iterator<Integer>() {
			
			private int currentIndex = 0;
			
			@Override
			public boolean hasNext() {
				return currentIndex < size;
			}

			@Override
			public Integer next() {
				return multiset[currentIndex++];
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();				
			}
		};
	}

	
	@Override
	public String toString() {
		String repr = "{";
		for (int i = 0; i < multiset.length; i++){
			repr += multiset[i];
			
			if (i != multiset.length - 1){
				repr += ", ";
			}
		}
		repr += "}";
		return repr;
	}
}

