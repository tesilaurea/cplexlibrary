package org.uniroma2.tesi.codd.placement;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloModeler;
import ilog.concert.IloNumExpr;
import ilog.concert.IloObjective;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplexModeler;

import java.util.List;

import org.uniroma2.tesi.codd.ODDException;
import org.uniroma2.tesi.codd.ODDParameters;
import org.uniroma2.tesi.codd.model.MSoaEdge;
import org.uniroma2.tesi.codd.model.MSoaGraph;
import org.uniroma2.tesi.codd.model.MSoaPath;
import org.uniroma2.tesi.codd.model.MSoaVertex;
import org.uniroma2.tesi.codd.model.NewPlacementX;
import org.uniroma2.tesi.codd.model.NewPlacementY;
import org.uniroma2.tesi.codd.model.OptimalSolution;
import org.uniroma2.tesi.codd.model.ServiceEdge;
import org.uniroma2.tesi.codd.model.ServiceGraph;
import org.uniroma2.tesi.codd.model.ServiceVertex;

public class ODDServiceModel implements ODDModel {
	
	protected static final boolean DEBUG = true;
	
	protected ODDParameters params;
	
	protected MSoaGraph dspGraph;
	protected ServiceGraph resGraph;
	protected IloCplex cplex;
	protected NewPlacementX X;
	protected NewPlacementY Y;
	protected OptimalSolution solution;
	
	protected IloNumExpr R;
	protected IloNumExpr logA;
	
	private long compilationTime;
	
	public ODDServiceModel(MSoaGraph dspGraph, ServiceGraph resGraph,
			ODDParameters parameters) throws ODDException {
		
		this.dspGraph = dspGraph;
		this.resGraph = resGraph;
		
		this.params = parameters;
		
		this.solution = null;
		this.R = null;
		this.logA = null;
		this.X = null;
		this.Y = null;
		this.compilationTime = Long.MAX_VALUE;
		
		try {
			this.cplex = new IloCplex();
		} catch (IloException exc) {
			throw new ODDException("Error while creating model: " + exc.getMessage());
		}
	}
	
	public ODDServiceModel(MSoaGraph dspGraph, ServiceGraph resGraph) throws ODDException {
		this(dspGraph, resGraph, new ODDParameters(ODDModel.MODE.BASIC));
	}

	public MSoaGraph getDspGraph() {
		return dspGraph;
	}

	public ServiceGraph getResGraph() {
		return resGraph;
	}

	public void compile() throws ODDException{
		
		IloModeler modeler = new IloCplexModeler();	

		compilationTime = System.currentTimeMillis();
		
		/********************************************************************************
		 * Decision Variables		
		 ********************************************************************************/
		try {
			this.X = new NewPlacementX(dspGraph, resGraph);
			this.Y = new NewPlacementY(dspGraph, resGraph);
		} catch (IloException exc) {
			throw new ODDException("Error while defining decision variables X and Y: " + exc.getMessage());
		}	
		
				
		/********************************************************************************
		 * Response-Time		
		 ********************************************************************************/
		try {
			R = cplex.numVar(0, Double.MAX_VALUE, "R");
			for (MSoaPath path : dspGraph.getPaths()) {

				/* R_computation */
				IloLinearNumExpr Rpex = modeler.linearNumExpr();	
				for(Integer i : path.getNodesIndexes()){
					MSoaVertex in = dspGraph.getVertices().get(i);
					
					for(ServiceVertex vn : resGraph.getVertices().values()){
						
						// questo è da verificare 
						
						if (in.isDeployableOn(vn.getIndex())){
							int u = vn.getIndex();
							//il tempo di esecuzione è quello del 
							//servizio astratto, credo per definire
							//giustamente una equazione per ogni path,
							//che tenga conto dell'apporto di ogni path
							//( dato che risolviamo il problema del chaining
							//  dei servizi, per ora il path è uno!)
							//perciò quello che cambia è la latenza
							// n.b. lo speedup avrebbe senso se fosse 
							// definito a priori attraverso un test,
							// e se la macchine fossero diversi, 
							// noi per assunzione usiamo istanze 
							// AWS t2.micro (senza perdita di generalità
							// basterebbe riceverlo dall'esterno.. il punto
							// è che risolvendo pipeline per noi il vincolo
							// sempre quello è)
							Rpex.addTerm(in.getExecutionTime() / 
									vn.getSpeedup(), X.get(i, u));							
						}
					}
				}
				
				/* R_comunication*/
				IloLinearNumExpr Rptx = modeler.linearNumExpr();
				List<Integer> sequence = path.getNodesIndexes();
				for (int k = 0; k < sequence.size() - 1; k++) {
					Integer ik = sequence.get(k);
					Integer ik1 = sequence.get(k + 1);
					for (ServiceEdge eRes : resGraph.getEdges().values()) {
						int u = eRes.getFrom();
						int v = eRes.getTo();
						MSoaVertex ikDsp = dspGraph.getVertices().get(ik);
						MSoaVertex ik1Dsp = dspGraph.getVertices().get(ik1);
						
						if(ikDsp.isDeployableOn(u) && ik1Dsp.isDeployableOn(v)){
							Rptx.addTerm(eRes.getDelay(), Y.get(ik, ik1, u, v));
						}
					}
				}					
				
				/* XXX: ref.18082015, Eq.12 */
				IloNumExpr Rp = modeler.sum(Rpex, Rptx);
				cplex.addLe(Rp, R, "objBound_R");
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Response-Time: " + exc.getMessage());
		}
		
		/********************************************************************************
		 * Availability		
		 ********************************************************************************/
		try {
			logA  = modeler.numVar(-Double.MAX_VALUE, 0, "logA");
			
			/* A nodes */
			IloLinearNumExpr logAex = modeler.linearNumExpr();
			for (MSoaVertex iDsp : dspGraph.getVertices().values()) {
				for (ServiceVertex uRes : resGraph.getVertices().values()) {
					
					if (iDsp.isDeployableOn(uRes.getIndex())){
						int i = iDsp.getIndex();
						int u = uRes.getIndex();
						logAex.addTerm(Math.log(uRes.getAvailability()), X.get(i, u));
					}
				}					
			}				
			
			/* A links */
			IloLinearNumExpr logAtx = modeler.linearNumExpr();
			for (MSoaEdge ijDsp : dspGraph.getEdges().values()) {
				for (ServiceEdge uvRes: resGraph.getEdges().values()) {
					int i = ijDsp.getFrom();
					int j = ijDsp.getTo();
					int u = uvRes.getFrom();
					int v = uvRes.getTo();
					
					MSoaVertex iDsp = dspGraph.getVertices().get(i);
					MSoaVertex jDsp = dspGraph.getVertices().get(j);
					
					if(iDsp.isDeployableOn(u) && jDsp.isDeployableOn(v)){
						logAtx.addTerm(Math.log(uvRes.getAvailability()), 
								Y.get(i, j, u, v));
					}
				}					
			}
			IloNumExpr logAx = modeler.sum(logAex, logAtx);
			//??? nel paper è >= logA
			cplex.addEq(logAx, logA, "objBound_A");
			
			
			
		} catch (IloException exc) {
			throw new ODDException("Error while defining Availability: " + exc.getMessage());
		}
		
		/********************************************************************************
		 * Objective
		 ********************************************************************************/	
		IloObjective obj;
		IloNumExpr objRExpr, objAExpr, objExpr;
		try {			
			objRExpr = modeler.prod(modeler.sum(params.getRmax(), modeler.negative(R)), params.getWeightRespTime() / (params.getRmax() - params.getRmin()));
			objAExpr = modeler.prod(modeler.sum(logA, (-Math.log(params.getAmin()))), 	params.getWeightAvailability() / (Math.log(params.getAmax()) - Math.log(params.getAmin())));
			objExpr  = modeler.sum(objRExpr, objAExpr);
			obj 	 = modeler.maximize(objExpr);
			cplex.addObjective(obj.getSense(), obj.getExpr(), "Fx");		
		} catch (IloException exc) {
			throw new ODDException("Error while defining Objective Function: " + exc.getMessage());
		}
		
		//questa mi sa che non ce l'ho
		//E' un bound per la soluzione per cui si può collocare i servizi rispettando
		//il limite della capacità
		//Il mio modello è diverso da quello in uso qui
		
		
		/********************************************************************************
		 * Capacity Bound - Eq.15
		 ********************************************************************************/
		/*try {
			for (ResourceVertex uRes : resGraph.getVertices().values()) {
				IloLinearNumExpr deployedOpCapacity = modeler.linearNumExpr();
				for (MSoaVertex iDsp : dspGraph.getVertices().values()) {
					int i = iDsp.getIndex();
					int u = uRes.getIndex();
					
					if (iDsp.deployableOn(u)){
						deployedOpCapacity.addTerm(iDsp.getRequiredResources(), X.get(i, u));
					}
				}					
				cplex.addLe(deployedOpCapacity, uRes.getAvailableResources(), 
						"capb_res_" + uRes.getIndex());
			}			
		} catch (IloException exc) {
			throw new ODDException("Error while defining Capacity Bound: " + exc.getMessage());
		}*/
		
		//(dovrebbe essere il eq. 25)
		/********************************************************************************
		 * Uniqueness Bound - Eq.16
		 ********************************************************************************/		
		try {
			for (MSoaVertex iDsp : dspGraph.getVertices().values()) {
				IloLinearNumExpr exprUniqueness = modeler.linearNumExpr();
				for (ServiceVertex uRes : resGraph.getVertices().values()) {
					int i = iDsp.getIndex();
					int u = uRes.getIndex();
					
					if (iDsp.isDeployableOn(u)){
						exprUniqueness.addTerm(1.0, X.get(i, u));
					}
				}
				cplex.addEq(exprUniqueness, 1.0, 
						"uniqb_dsp_" + iDsp.getIndex());
			}			
		} catch (IloException exc) {
			throw new ODDException("Error while defining Uniqueness Bound: " + exc.getMessage());
		}
		
		//sono le eq (26)
		/********************************************************************************
		 * Connectivity Bound - Eq.28, 29 - review 25.09.2015 
		 ********************************************************************************/
		try {
			for (MSoaEdge ijDsp : dspGraph.getEdges().values()) {
				//trovo vertici arco 
				int i = ijDsp.getFrom();
				int j = ijDsp.getTo();
				MSoaVertex iDsp = dspGraph.getVertices().get(i);
				MSoaVertex jDsp = dspGraph.getVertices().get(j);
			
				for (ServiceVertex uRes : resGraph.getVertices().values()) {
					int u = uRes.getIndex();
					
					if(iDsp.isDeployableOn(u)){
						IloLinearNumExpr exprConn = modeler.linearNumExpr();
						for (ServiceVertex vRes : resGraph.getVertices().values()) {
							int v = vRes.getIndex();
							
							if(jDsp.isDeployableOn(v)){
								if (Y.get(i, j, u, v) != null)
									exprConn.addTerm(1.0, Y.get(i, j, u, v));
							}						
						}
						cplex.addEq(X.get(i, u), exprConn, "conb1_" + i + "," + j);
					}						
				}
			}
		
			for (MSoaEdge ijDsp : dspGraph.getEdges().values()) {
				int i = ijDsp.getFrom();
				int j = ijDsp.getTo();
				MSoaVertex iDsp = dspGraph.getVertices().get(i);
				MSoaVertex jDsp = dspGraph.getVertices().get(j);
				
				for (ServiceVertex vRes : resGraph.getVertices().values()) {
					int v = vRes.getIndex();
					
					if(jDsp.isDeployableOn(v)){
						IloLinearNumExpr exprConn = modeler.linearNumExpr();
						for (ServiceVertex uRes : resGraph.getVertices().values()) {
							int u = uRes.getIndex();
							
							if(iDsp.isDeployableOn(u)){
								if (Y.get(i, j, u, v) != null)
									exprConn.addTerm(1.0, Y.get(i, j, u, v));
							}	
						}
						cplex.addEq(X.get(j, v), exprConn, "conb2_" + i + "," + j);	
					}
				}
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Connectivity Bound: " + exc.getMessage());
		}	
		compilationTime = System.currentTimeMillis() - compilationTime;

		
		/* XXX: DEBUG */ 
		try {
			cplex.exportModel("lpex2.lp");
		} catch (IloException e) {
			throw new ODDException("Error while exporting ODDModel on file: " + e.getMessage());
		}
		
	}
	
	public void exportModel() throws ODDException{
		/* XXX: DEBUG */ 
		try {
			cplex.exportModel("lpex2.lp");
		} catch (IloException e) {
			throw new ODDException("Error while exporting ODDModel on file: " + e.getMessage());
		}
	}
	
	public OptimalSolution solve() throws ODDException{
		
		if (R == null)
			compile();
		
		try {
			if (params.isUseTimeLimit()){
				cplex.setParam(IloCplex.IntParam.TimeLimit, params.getTimeLimit()); //expressed in seconds 
				
			}
			if (params.isDefineOptimalGap()){			
				cplex.setParam(IloCplex.DoubleParam.EpGap, params.getGap());
			}
			
			solution = new OptimalSolution(dspGraph.getVertices().size());
			
			if (!DEBUG){
				cplex.setOut(null);
				cplex.setWarning(null);
			}
			
			long enlapsedTime = System.currentTimeMillis();
			if(cplex.solve()){
				enlapsedTime = System.currentTimeMillis() - enlapsedTime;
				
			    solution.setOptObjValue(cplex.getObjValue());
			    solution.setOptR(cplex.getValue(R));
			    solution.setOptLogA(cplex.getValue(logA));
			    solution.setResolutionTime(enlapsedTime);
			    solution.setCompilationTime(compilationTime);

			    for (MSoaVertex dspOperator : dspGraph.getVertices().values()) {
					for (ServiceVertex resNode : resGraph.getVertices().values()) {
						int i = dspOperator.getIndex();
						int u = resNode.getIndex();
						double xval = 0;
						if (dspOperator.isDeployableOn(u))
							xval = cplex.getValue(X.get(i, u));
			        	if (xval > 0)
			        		solution.setPlacement(i, u);
					}				
				}		

				// cplex.writeSolution("solution.sol");

			} else {
				System.out.println("Unable to find a solution");
			}

			solution.setStatus(cplex.getStatus());

		} catch (IloException e) {
			throw new ODDException("Error while solving OPP Model: " + e.getMessage());
		}
		
		return solution;
	}
	
	public void clean() {
		if (cplex != null)
			cplex.end();
	}
	
	@Override
	public String type() {
		return "BASIC";
	}

	@Override
	public void pin(int iDsp, int uRes){

		try {
		
			if (X.get(iDsp, uRes) != null)
				cplex.addEq(X.get(iDsp, uRes), 1, "pinned_" + iDsp + "," + uRes);
			else
				System.out.println("Invalid pinning");
			
		} catch (IloException e) {
			e.printStackTrace();
		}	
		
	}

	@Override
	public String toString() {
		return "ODDServiceModel [params=" + params + ", dspGraph=" + dspGraph + ", resGraph=" + resGraph + ", cplex="
				+ cplex + ", X=" + X + ", Y=" + Y + ", solution=" + solution + ", R=" + R + ", logA=" + logA
				+ ", compilationTime=" + compilationTime + "]";
	}
	
	
}
