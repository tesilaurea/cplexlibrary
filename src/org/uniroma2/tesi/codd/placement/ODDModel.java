package org.uniroma2.tesi.codd.placement;

import org.uniroma2.tesi.codd.ODDException;
import org.uniroma2.tesi.codd.model.OptimalSolution;

public interface ODDModel {
	
	public enum MODE{
		BANDWIDTH,
		BASIC
	};
	
	public void compile() throws ODDException;
	
	public OptimalSolution solve() throws ODDException;
	
	public void clean();
	
	public String type();
	
	public void pin(int dspIndex, int resIndex);
}
