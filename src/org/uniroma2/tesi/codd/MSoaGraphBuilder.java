package org.uniroma2.tesi.codd;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.uniroma2.tesi.codd.metricsprovider.ApplicationMetricsProvider;
import org.uniroma2.tesi.codd.model.MSoaEdge;
import org.uniroma2.tesi.codd.model.MSoaGraph;
import org.uniroma2.tesi.codd.model.MSoaPath;
import org.uniroma2.tesi.codd.model.MSoaVertex;
import org.uniroma2.tesi.codd.model.Pair;
import org.uniroma2.tesi.codd.model.ResourceGraph;
import org.uniroma2.tesi.codd.model.ResourceVertex;
import org.uniroma2.tesi.codd.model.ServiceEdge;
import org.uniroma2.tesi.codd.model.ServiceGraph;
import org.uniroma2.tesi.codd.model.ServiceVertex;

public class MSoaGraphBuilder {
	
	private final long randomSeed = 201512011037l;
	public enum TYPE {
		SEQUENTIAL, FAT, MULTILEVEL, UNKNOWN, CUSTOM
	};
	protected LinkedList<String> servicesList =  new LinkedList<String>();

	private int NUMBER_SERVICES = 5;
	protected TYPE type;
	protected MSoaGraph msoaGraph;

	public MSoaGraphBuilder() {
		msoaGraph = null;
		this.type = TYPE.UNKNOWN;
	}

	public MSoaGraphBuilder(MSoaGraph msoaGraph) {
		this.msoaGraph = msoaGraph;
		this.type = TYPE.UNKNOWN;
	}

	public MSoaGraph getGraph() {
		return msoaGraph;
	}

	public MSoaGraphBuilder create(
			// TopologyDetails topology, GeneralTopologyContext topologyContext,
			ApplicationMetricsProvider metricsProvider, TYPE type,
			int numOperators) throws ODDException {

		if (numOperators < 2)
			throw new ODDException("Invalid number of operators");
		
		this.type = type;
		
		switch(type){
			case SEQUENTIAL:
				return createLongTopology(metricsProvider, numOperators);
				
			case CUSTOM:
				return createCustomTopology(metricsProvider, numOperators);
			case FAT: 
				return createFatTopology(metricsProvider, numOperators);
	
			case MULTILEVEL:
				return createMultilevelTopology(metricsProvider, numOperators);
			
			default:
				return createLongTopology(metricsProvider, numOperators);
		}

	}

	public TYPE getType() {
		return type;
	}
	
	public MSoaGraphBuilder restrictPlacement(ResourceGraph res, double percentage){
		
		Collection<MSoaVertex> operators = msoaGraph.getVertices().values();
		Collection<ResourceVertex> resources = res.getVertices().values();
		
		if (percentage > 1.0)
			percentage = 1;
		
		//Non mi convince
		Random rnd = new Random(randomSeed);
		
		for (MSoaVertex i : operators){
			for (ResourceVertex u : resources){
				if(rnd.nextDouble() <= percentage){
					i.addCandidates(u.getIndex());
				}
			}
		}
		
		return this;
		
	}

	public MSoaGraphBuilder restrictPlacement(Set<Integer> resNodes){
		
		Collection<MSoaVertex> operators = msoaGraph.getVertices().values();

		for (MSoaVertex i : operators){
			for (Integer u : resNodes){
				i.addCandidates(u.intValue());
			}
		}
	
		return this;
		
	}
	

	
	/**
	 * This topology has: 
	 *  - 1 source (first level)
	 *  - (2n/3) operators in the second level
	 *  - n/3 operators in the third level 
	 *  - 1 sink operator (forth level)
	 *  
	 *  where n is equal to numOperators - 2
	 *  
	 * @param metricsProvider
	 * @param numOperators
	 * @return
	 * @throws ODDException
	 */
	private MSoaGraphBuilder createMultilevelTopology(
			ApplicationMetricsProvider metricsProvider,
			int numOperators) throws ODDException {
		
		Map<Integer, MSoaVertex> vDsp = new HashMap<Integer, MSoaVertex>();
		Map<Pair, MSoaEdge> eDsp = new HashMap<Pair, MSoaEdge>();
		List<Integer> sourcesIndex = new ArrayList<Integer>();
		List<Integer> sinksIndex = new ArrayList<Integer>();

		// Source and sink are not replicated 

		for (int i = 0; i < numOperators; i++) {
			int requiredResource = metricsProvider.getRequiredResources(i);
			double executionTime = metricsProvider.getExecutionTime(i);

			double cost = metricsProvider.getCost(i);
			double serviceRate = metricsProvider.getServiceRate(i);
			double codeImageSize = metricsProvider.getCodeImageSize(i);
			double internalStateImageSize = metricsProvider.getInternalStateImageSize(i);
			
			MSoaVertex v = new MSoaVertex(i, "op_" + i, "name", requiredResource, executionTime, cost, internalStateImageSize, codeImageSize);
			v.setServiceRate(serviceRate);
			vDsp.put(new Integer(i), v);
		}

		/* 2. Set node 0 as source */
		sourcesIndex.add(new Integer(0));

		/* 3. Create DspEdges to represent link between executors */
		int numIntOperators = numOperators - 2;
		int numOperSecondLevel = (int) Math.floor(2.0 * ((double) numIntOperators / 3.0));
		int numOperThirdLevel = numIntOperators - numOperSecondLevel;
		if (numOperThirdLevel < 0){
			numOperThirdLevel = 1;
			numOperSecondLevel = numIntOperators - numOperThirdLevel;
		}
		int[] prevLevel = new int[numOperSecondLevel];
		int sinkId = numOperators - 1;
		
		/* edges for second level operators */
		for (int j = 1; j < numOperSecondLevel + 1; j++){	
			int i = 0;
			MSoaEdge ij = new MSoaEdge(i, j);
			eDsp.put(new Pair(i, j), ij);
			prevLevel[(j - 1)] = j;			
		}

//		for (int i = 0; i < prevLevel.length; i++)
//			System.out.println("->>> prevlevel["+i + "]" + prevLevel[i]);

		/* edges for third level operators */
		for (int j = numOperSecondLevel + 1; j < numOperSecondLevel + numOperThirdLevel + 1; j++){	
			for(int i : prevLevel){
				MSoaEdge ij = new MSoaEdge(i, j);
				eDsp.put(new Pair(i, j), ij);
			}
		}

		prevLevel = new int[numOperThirdLevel];
		for (int j = numOperSecondLevel + 1; j < numOperSecondLevel + numOperThirdLevel + 1; j++){	
			prevLevel[(j - numOperSecondLevel - 1)] = j;			
		}
//		for (int i = 0; i < prevLevel.length; i++)
//			System.out.println("->>> prevlevel["+i + "]" + prevLevel[i]);

		
		
		/* edges for non-replicated operators */
		for(int prevId : prevLevel){
			MSoaEdge ij = new MSoaEdge(prevId, sinkId);
			eDsp.put(new Pair(prevId, sinkId), ij);
		}
		
			
		
		/* 4. Compute Paths */
		ArrayList<MSoaPath> paths = computePaths(vDsp, eDsp, sourcesIndex);

		/* 5. Identify sinks */
		for (MSoaPath path : paths) {
			sinksIndex.add(path.getSink());
		}

		/* 5. Creating object DspGraph */
		String graphId = "multilevel-topology";
		msoaGraph = new MSoaGraph(graphId, vDsp, eDsp);
		msoaGraph.setReconfigurationSyncTime(metricsProvider.getReconfigurationSyncTime());
		msoaGraph.setPaths(paths);

		for (Integer so : sourcesIndex)
			msoaGraph.addSource(so);

		for (Integer si : sinksIndex)
			msoaGraph.addSink(si);

		return this;
	}
	
	
	public static Map<Integer, MSoaVertex> createExternalMSoaVertexMap(
			LinkedList<MSoaVertex> msoaVertexList){
		Map<Integer, MSoaVertex> vRes = new HashMap<Integer, MSoaVertex>();
		for(int index = 0; index< msoaVertexList.size(); index++){
			vRes.put(new Integer(index), msoaVertexList.get(index));
		}
		return vRes;
	}
	
	public static Map<Pair, MSoaEdge> createExternalMSoaEdgeMap(
			LinkedList<MSoaEdge> msoaEdgeList){
		Map<Pair, MSoaEdge> eRes = new HashMap<Pair, MSoaEdge>();
		for(int index = 0; index< msoaEdgeList.size(); index++){
			MSoaEdge e = msoaEdgeList.get(index);
			eRes.put(new Pair(e.getFrom(), e.getTo()), e);
		}
		return eRes;
	}
	
	public MSoaGraphBuilder createExternalTopology(
			Map<Integer, MSoaVertex> vDsp,
			Map<Pair, MSoaEdge> eDsp, 
			List<Integer> sourcesIndex,
			List<Integer> sinksIndex,
			ArrayList<MSoaPath> paths){
		
		/* 5. Creating object DspGraph */
		String graphId = "long-topology";
		msoaGraph = new MSoaGraph(graphId, vDsp, eDsp);
		msoaGraph.setPaths(paths);
		//potrebbe essere anche esternalizzato o tolto proprio
		/*msoaGraph.setReconfigurationSyncTime(
				metricsProvider.getReconfigurationSyncTime());*/

		for (Integer so : sourcesIndex)
			msoaGraph.addSource(so);

		for (Integer si : sinksIndex)
			msoaGraph.addSink(si);

		return this;
		
	}
	private MSoaGraphBuilder createCustomTopology(
		ApplicationMetricsProvider metricsProvider,
		int numOperators) throws ODDException {
			Map<Integer, MSoaVertex> vDsp = new HashMap<Integer, MSoaVertex>();
			Map<Pair, MSoaEdge> eDsp = new HashMap<Pair, MSoaEdge>();
			List<Integer> sourcesIndex = new ArrayList<Integer>();
			List<Integer> sinksIndex = new ArrayList<Integer>();
			
			//controllare che sia rispettato numOperators
			
			//service1
			createVertex(0, metricsProvider, vDsp, 0);
			//service2
			createVertex(3, metricsProvider, vDsp, 1);
			//service3
			createVertex(4, metricsProvider, vDsp, 2);
			//service0
			createVertex(1, metricsProvider, vDsp, 3);
			//service1
			createVertex(2, metricsProvider, vDsp, 4);
			
			
			/* 2. Set node 0 as source */
			sourcesIndex.add(new Integer(0));

			/* 3. Create DspEdges to represent link between executors */
			int[] prevLevel = new int[] { 0 };
			for (int j = 1; j < numOperators; j++) {
				for (int i : prevLevel) {
					MSoaEdge ij = new MSoaEdge(i, j);
					eDsp.put(new Pair(i, j), ij);
				}
				prevLevel = new int[] { j };
			}

			/* 4. Compute Paths */
			ArrayList<MSoaPath> paths = computePaths(vDsp, eDsp, sourcesIndex);

			/* 5. Identify sinks */
			for (MSoaPath path : paths) {
				sinksIndex.add(path.getSink());
			}

			/* 5. Creating object DspGraph */
			String graphId = "long-topology";
			msoaGraph = new MSoaGraph(graphId, vDsp, eDsp);
			msoaGraph.setPaths(paths);
			//potrebbe essere anche esternalizzato o tolto proprio
			msoaGraph.setReconfigurationSyncTime(
					metricsProvider.getReconfigurationSyncTime());

			for (Integer so : sourcesIndex)
				msoaGraph.addSource(so);

			for (Integer si : sinksIndex)
				msoaGraph.addSink(si);

			return this;
			
			
	}
	public  static MSoaVertex createVertex(		
			String type,
			ApplicationMetricsProvider metricsProvider, 
			int index , String name, String microservice){
		//service1
		//String type = "Service" + Integer.toString(service);
		int requiredResource = metricsProvider.getRequiredResources(index);
		double executionTime = metricsProvider.getExecutionTime(index); // questo campo
			// è strano!!!!!
	
		System.out.println(executionTime);
		double cost = metricsProvider.getCost(index);
		double serviceRate = metricsProvider.getServiceRate(index);
		double codeImageSize = metricsProvider.getCodeImageSize(index);
		double internalStateImageSize = metricsProvider.getInternalStateImageSize(index);
		MSoaVertex v = new MSoaVertex(index, "op_" + Integer.toString(index), name,
				requiredResource, 
				executionTime, 
				cost, 
				internalStateImageSize, 
				codeImageSize);
		v.setType(type);
		v.setServiceRate(serviceRate);
		v.setMicroservice(microservice);
		return v;
	}
	
	public  static MSoaVertex createVertex(		
			String type,
			ApplicationMetricsProvider metricsProvider, 
			int index , String name){
		//service1
		//String type = "Service" + Integer.toString(service);
		int requiredResource = metricsProvider.getRequiredResources(index);
		double executionTime = metricsProvider.getExecutionTime(index); // questo campo
			// è strano!!!!!
	
		System.out.println(executionTime);
		double cost = metricsProvider.getCost(index);
		double serviceRate = metricsProvider.getServiceRate(index);
		double codeImageSize = metricsProvider.getCodeImageSize(index);
		double internalStateImageSize = metricsProvider.getInternalStateImageSize(index);
		MSoaVertex v = new MSoaVertex(index, "op_" + Integer.toString(index), name,
				requiredResource, 
				executionTime, 
				cost, 
				internalStateImageSize, 
				codeImageSize);
		v.setType(type);
		v.setServiceRate(serviceRate);
		
		return v;
	}
	
	public  static MSoaVertex createVertex(int service, 
			ApplicationMetricsProvider metricsProvider, int index ){
		//service1
		String type = "Service" + Integer.toString(service);
		int requiredResource = metricsProvider.getRequiredResources(index);
		double executionTime = metricsProvider.getExecutionTime(index);
	
		System.out.println(executionTime);
		double cost = metricsProvider.getCost(index);
		double serviceRate = metricsProvider.getServiceRate(index);
		double codeImageSize = metricsProvider.getCodeImageSize(index);
		double internalStateImageSize = metricsProvider.getInternalStateImageSize(index);
		MSoaVertex v = new MSoaVertex(index, "op_" + Integer.toString(index), "name",
				requiredResource, 
				executionTime, 
				cost, 
				internalStateImageSize, 
				codeImageSize);
		v.setType(type);
		v.setServiceRate(serviceRate);
		
		return v;
	}
	void createVertex(int service, 
			ApplicationMetricsProvider metricsProvider, 
			Map<Integer, MSoaVertex> vDsp, int index ){
		//service1
		String type = "Service" + Integer.toString(service);
		int requiredResource = metricsProvider.getRequiredResources(index);
		double executionTime = metricsProvider.getExecutionTime(index);
	
		System.out.println(executionTime);
		double cost = metricsProvider.getCost(index);
		double serviceRate = metricsProvider.getServiceRate(index);
		double codeImageSize = metricsProvider.getCodeImageSize(index);
		double internalStateImageSize = metricsProvider.getInternalStateImageSize(index);
		MSoaVertex v = new MSoaVertex(index, "op_" + Integer.toString(index), "name",
				requiredResource, 
				executionTime, 
				cost, 
				internalStateImageSize, 
				codeImageSize);
		v.setType(type);
		v.setServiceRate(serviceRate);
		vDsp.put(new Integer(index), v);
	
	}
	private MSoaGraphBuilder createLongTopology(
			ApplicationMetricsProvider metricsProvider,
			int numOperators) throws ODDException {
		/* SEQUENTIAL layered topology */
		Map<Integer, MSoaVertex> vDsp = new HashMap<Integer, MSoaVertex>();
		Map<Pair, MSoaEdge> eDsp = new HashMap<Pair, MSoaEdge>();
		List<Integer> sourcesIndex = new ArrayList<Integer>();
		List<Integer> sinksIndex = new ArrayList<Integer>();
		setServicesList(NUMBER_SERVICES);
		/* Source and sink are not replicated */

		/* 1. Create DspVertex */
		for (int i = 0; i < numOperators; i++) {
			
			String type = randType(i);
			int requiredResource = metricsProvider.getRequiredResources(i);
			double executionTime = metricsProvider.getExecutionTime(i);

			System.out.println(executionTime);
			double cost = metricsProvider.getCost(i);
			double serviceRate = metricsProvider.getServiceRate(i);
			double codeImageSize = metricsProvider.getCodeImageSize(i);
			double internalStateImageSize = metricsProvider.getInternalStateImageSize(i);
			
			MSoaVertex v = new MSoaVertex(i, "op_" + i, "name",
					requiredResource, 
					executionTime, 
					cost, 
					internalStateImageSize, 
					codeImageSize);
			v.setType(type);
			v.setServiceRate(serviceRate);
			vDsp.put(new Integer(i), v);
		}

		/* 2. Set node 0 as source */
		sourcesIndex.add(new Integer(0));

		/* 3. Create DspEdges to represent link between executors */
		int[] prevLevel = new int[] { 0 };
		for (int j = 1; j < numOperators; j++) {
			for (int i : prevLevel) {
				MSoaEdge ij = new MSoaEdge(i, j);
				eDsp.put(new Pair(i, j), ij);
			}
			prevLevel = new int[] { j };
		}

		/* 4. Compute Paths */
		ArrayList<MSoaPath> paths = computePaths(vDsp, eDsp, sourcesIndex);

		/* 5. Identify sinks */
		for (MSoaPath path : paths) {
			sinksIndex.add(path.getSink());
		}

		/* 5. Creating object DspGraph */
		String graphId = "long-topology";
		msoaGraph = new MSoaGraph(graphId, vDsp, eDsp);
		msoaGraph.setPaths(paths);
		//potrebbe essere anche esternalizzato o tolto proprio
		msoaGraph.setReconfigurationSyncTime(
				metricsProvider.getReconfigurationSyncTime());

		for (Integer so : sourcesIndex)
			msoaGraph.addSource(so);

		for (Integer si : sinksIndex)
			msoaGraph.addSink(si);

		return this;
	}

	private MSoaGraphBuilder createFatTopology(
			ApplicationMetricsProvider metricsProvider,
			int numOperators) throws ODDException {
		
		
		/* FAT topology */
		Map<Integer, MSoaVertex> vDsp = new HashMap<Integer, MSoaVertex>();
		Map<Pair, MSoaEdge> eDsp = new HashMap<Pair, MSoaEdge>();
		List<Integer> sourcesIndex = new ArrayList<Integer>();
		List<Integer> sinksIndex = new ArrayList<Integer>();

		/* Source and sink are not replicated */

		/* 1. Create DspVertex */
		for (int i = 0; i < numOperators; i++) {
			int requiredResource = metricsProvider.getRequiredResources(i);
			double executionTime = metricsProvider.getExecutionTime(i);
			double cost = metricsProvider.getCost(i);
			double serviceRate = metricsProvider.getServiceRate(i);
			double codeImageSize = metricsProvider.getCodeImageSize(i);
			double internalStateImageSize = metricsProvider.getInternalStateImageSize(i);
			
			MSoaVertex v = new MSoaVertex(i, "op_" + i, "name",requiredResource,
					executionTime, cost, internalStateImageSize, codeImageSize);
			v.setServiceRate(serviceRate);
			vDsp.put(new Integer(i), v);
		}

		/* 2. Set node 0 as source */
		sourcesIndex.add(new Integer(0));

		/* 3. Create DspEdges to represent link between executors */
		int numIntOperators = numOperators - 1;
		int[] prevLevel = new int[numIntOperators - 1];
		int sinkId = numOperators - 1;
		
		/* edges for replicated operators */
		for (int j = 1; j < numIntOperators; j++){	
			int i = 0;
			MSoaEdge ij = new MSoaEdge(i, j);
			eDsp.put(new Pair(i, j), ij);
			prevLevel[(j - 1)] = j;
			
		}

		/* edges for non-replicated operators */
		for(int prevId : prevLevel){
			MSoaEdge ij = new MSoaEdge(prevId, sinkId);
			eDsp.put(new Pair(prevId, sinkId), ij);
		}
		
			
		/* 4. Compute Paths */
		ArrayList<MSoaPath> paths = computePaths(vDsp, eDsp, sourcesIndex);

		/* 5. Identify sinks */
		for (MSoaPath path : paths) {
			sinksIndex.add(path.getSink());
		}

		/* 5. Creating object DspGraph */
		String graphId = "long-topology";
		msoaGraph = new MSoaGraph(graphId, vDsp, eDsp);
		msoaGraph.setPaths(paths);
		msoaGraph.setReconfigurationSyncTime(metricsProvider.getReconfigurationSyncTime());

		for (Integer so : sourcesIndex)
			msoaGraph.addSource(so);

		for (Integer si : sinksIndex)
			msoaGraph.addSink(si);

		return this;
	}
	
	public ArrayList<MSoaPath> computePaths(Map<Integer, MSoaVertex> vDsp,
			Map<Pair, MSoaEdge> eDsp, List<Integer> sourcesIndex) {
		/* 3. Compute paths between source and sinks */
		/* Creating paths... */
		List<Integer> frontier = new ArrayList<Integer>();

		/* Create dumb paths from source to source itself */
		ArrayList<MSoaPath> paths = new ArrayList<MSoaPath>();
		for (Integer so : sourcesIndex) {
			MSoaPath path = new MSoaPath(so);
			paths.add(path);
		}
//---------> sicuro che funziona per ogni source ? 
		if (sourcesIndex.size() > 0) {
			/* Initialize graph exploration variables */
			/* Current node must be a source of the DSP Graph */
			Integer currentNode = sourcesIndex.get(0);
			boolean modified = true;

			do {

				modified = false;
				List<Pair> oEdges = getOutgoingEdges(currentNode, eDsp.keySet());

				/* Update nodes nodeTBV frontier */
				for (Pair p : oEdges) {
					frontier.add(new Integer(p.getB()));
				}

				ArrayList<MSoaPath> pathsToUpdate = getPathsEndingInNode(paths,
						currentNode);

				for (MSoaPath pathToUpdate : pathsToUpdate) {

					if (oEdges.isEmpty()) {
						continue;
					}

					paths.remove(pathToUpdate);

					for (Pair oe : oEdges) {
						MSoaPath newPath = pathToUpdate.clone();
						newPath.add(oe.getB());
						paths.add(newPath);
						modified = true;
					}
				}

				if (frontier.isEmpty()) {
					modified = false;
				} else {
					currentNode = frontier.remove(0);
				}
			} while (modified || !frontier.isEmpty());
		}

		return paths;
	}

	private List<Pair> getOutgoingEdges(int currentNodeId, Set<Pair> edges) {
		List<Pair> outgoingEdges = new ArrayList<Pair>();
		for (Pair e : edges)
			if (e.getA() == currentNodeId)
				outgoingEdges.add(e);
		return outgoingEdges;
	}

	private ArrayList<MSoaPath> getPathsEndingInNode(ArrayList<MSoaPath> paths,
			int nodeId) {

		ArrayList<MSoaPath> output = new ArrayList<MSoaPath>();
		for (MSoaPath path : paths) {
			if (path.isSink(nodeId))
				output.add(path);
		}
		return output;
	}

	public void printGraph() {

		System.out.println("DSP Graph");
		System.out.println("* Vertices ");
		for (MSoaVertex v : msoaGraph.getVertices().values()) {
			System.out.println("   " + v.toString());
		}

		System.out.println();
		System.out.println("* Edges");
		for (MSoaEdge e : msoaGraph.getEdges().values()) {
			System.out.println("   " + e.toString());
		}

		System.out.println();
		System.out.println(" --- ");
		System.out.print("* Sources\n   ");
		for (Integer so : msoaGraph.getSources()) {
			System.out.print(so + ", ");
		}
		System.out.println();
		System.out.print("* Sinks\n   ");
		for (Integer si : msoaGraph.getSinks()) {
			System.out.print(si + ", ");
		}
		System.out.println();
		System.out.println(" --- ");

		System.out.println();
		System.out.println("* Paths");
		for (MSoaPath path : msoaGraph.getPaths()) {
			System.out.println("> " + path.toString());
		}

		return;

	}
	
	public void setServicesList(int n){	
		for(int i=0; i<n; i++)
		{
			servicesList.add("Service"+Integer.toString(i));
		}		
	}
	public String randType(int i){		
		int index = i % NUMBER_SERVICES;
		return servicesList.get(index) ;
	}
	
	public void setCandidate(ResGraphBuilder rbuilder){
		ServiceGraph serGraph = rbuilder.getServiceGraph();
		for (MSoaVertex v : msoaGraph.getVertices().values()){
			for(ServiceVertex s: serGraph.getVertices().values()){
				if(s.getType().equals(v.getType())){
					v.addCandidates(s.getIndex());
				}
			}
		} 
	}

}
