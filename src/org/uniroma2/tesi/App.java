package org.uniroma2.tesi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.uniroma2.tesi.codd.MSoaGraphBuilder;
import org.uniroma2.tesi.codd.ODDException;
import org.uniroma2.tesi.codd.ODDParameters;
import org.uniroma2.tesi.codd.ResGraphBuilder;
import org.uniroma2.tesi.codd.metricsprovider.ApplicationMetricsProvider;
import org.uniroma2.tesi.codd.metricsprovider.ResourceMetricsProvider;
import org.uniroma2.tesi.codd.metricsprovider.SimpleApplicationMetricsProvider;
import org.uniroma2.tesi.codd.metricsprovider.SimpleResourceMetricsProvider;
import org.uniroma2.tesi.codd.model.MSoaEdge;
import org.uniroma2.tesi.codd.model.MSoaPath;
import org.uniroma2.tesi.codd.model.MSoaVertex;
import org.uniroma2.tesi.codd.model.OptimalSolution;
import org.uniroma2.tesi.codd.model.Pair;
import org.uniroma2.tesi.codd.model.ServiceEdge;
import org.uniroma2.tesi.codd.model.ServiceVertex;
import org.uniroma2.tesi.codd.placement.ODDModel;
import org.uniroma2.tesi.codd.placement.ODDModel.MODE;
import org.uniroma2.tesi.codd.placement.ODDServiceModel;
import org.uniroma2.tesi.codd.placement.ODDServiceModelV2;
import org.uniroma2.tesi.codd.placement.ODDServiceModelV3;
import org.uniroma2.tesi.codd.report.Report;
import org.uniroma2.tesi.codd.report.ReportException;

public class App 
{
    public static void main( String[] args )
    {
		System.out.println("Optimal MicroServices Orchestration library");
		//testMSOBasic();
		testMSO();
    }
    private static void testMSO(){
	    double weightAvailability = 0.5;
		double weightRespTime = 0.5;
		LinkedList<ServiceEdge> serviceEdgeList = 
				new LinkedList<ServiceEdge>();
		LinkedList<ServiceVertex> serviceVertexList = 
				new LinkedList<ServiceVertex>();
		
	
		ODDParameters params = new ODDParameters(MODE.BASIC);
		
		setParameters(params);
		
		//service 0 node 0
		serviceVertexList.add(ResGraphBuilder.createService(0, 0, 0, 0.6, 3));
		//service 1 node 1
		serviceVertexList.add(ResGraphBuilder.createService(1, 1, 1, 0.4, 3));
		//service 2 node 2
		serviceVertexList.add(ResGraphBuilder.createService(2, 2, 2, 0.8, 3));
		//service 3 node 0
		serviceVertexList.add(ResGraphBuilder.createService(3, 0, 3, 0.6, 3));
		//service 4 node 1
		serviceVertexList.add(ResGraphBuilder.createService(4, 1, 4, 0.9, 3));
		//service 4 node 0
		serviceVertexList.add(ResGraphBuilder.createService(4, 0, 5, 0.9, 3));
		//service 3 node 1
		serviceVertexList.add(ResGraphBuilder.createService(3, 1, 6, 0.1, 3));
		//service 0 node 2
		serviceVertexList.add(ResGraphBuilder.createService(0, 2, 7, 0.4, 3));
		//service 4 node 2
		serviceVertexList.add(ResGraphBuilder.createService(4, 2, 8, 0.7, 3));
		//service 3 node 2
		serviceVertexList.add(ResGraphBuilder.createService(3, 2, 9, 0.3, 3));
			
		ResourceMetricsProvider rmp = new SimpleResourceMetricsProvider(params);
		for (ServiceVertex u : serviceVertexList){
			for (ServiceVertex v : serviceVertexList){
				int uIndex = u.getIndex();
				int vIndex = v.getIndex();
	
				int latUV = (int) rmp.getSerLatency(u.getNode(), v.getNode());// getLat(u.getNode(), v.getNode()); //(int) metricsProvider.getLatency(uIndex, vIndex);
				double bigAUV = 1.0; // metricsProvider.getAvailability(uIndex, vIndex);
				
				ServiceEdge rEdge = new ServiceEdge(
						uIndex, 
						vIndex, latUV, bigAUV);
				serviceEdgeList.add(rEdge);
			}
		}
		
		
		/*****************************************************/
		 // RISORSE
		ResGraphBuilder rbuilder = new ResGraphBuilder();
		Map<Integer, ServiceVertex> vRes = 
				ResGraphBuilder.createExternalServiceVertexMap(
				serviceVertexList);
		Map<Pair, ServiceEdge> eRes = ResGraphBuilder.createExternalServiceEdgeMap(
				serviceEdgeList);
	    ResGraphBuilder r= rbuilder.createExternalServicePod(vRes, eRes);
	    
	    rbuilder.printSerGraph(true);
	    /*****************************************************/
	     //DAG
	    MSoaGraphBuilder.TYPE msoaType = MSoaGraphBuilder.TYPE.CUSTOM;
	    MSoaGraphBuilder mbuilder = new MSoaGraphBuilder();
	    LinkedList<MSoaEdge> msoaEdgeList = 
				new LinkedList<MSoaEdge>();
		LinkedList<MSoaVertex> msoaVertexList = 
				new LinkedList<MSoaVertex>();
		ApplicationMetricsProvider amp = new SimpleApplicationMetricsProvider(
				msoaType, 5, params);
		
		//service1
		msoaVertexList.add(MSoaGraphBuilder.createVertex(0, amp, 0));
		//service2
		msoaVertexList.add(MSoaGraphBuilder.createVertex(3, amp, 1));
		//service3
		msoaVertexList.add(MSoaGraphBuilder.createVertex(4, amp, 2));
		//service0
		//msoaVertexList.add(MSoaGraphBuilder.createVertex(1, amp, 3));
		//service1
		//msoaVertexList.add(MSoaGraphBuilder.createVertex(2, amp, 4));
		
	    Map<Integer, MSoaVertex> vDsp =  MSoaGraphBuilder.createExternalMSoaVertexMap(
	    		msoaVertexList);
		
		List<Integer> sourcesIndex = new ArrayList<Integer>();
		List<Integer> sinksIndex = new ArrayList<Integer>();
		
		/* 2. Set node 0 as source */
		sourcesIndex.add(new Integer(0));
	
		/* 3. Create DspEdges to represent link between executors */
		int[] prevLevel = new int[] { 0 };
		for (int j = 1; j < 3; j++) {
			for (int i : prevLevel) {
				MSoaEdge ij = new MSoaEdge(i, j);
				msoaEdgeList.add(ij);
			}
			prevLevel = new int[] { j };
		}
	
	
		
		Map<Pair, MSoaEdge> eDsp =  MSoaGraphBuilder.createExternalMSoaEdgeMap(
	    		msoaEdgeList);
		/* 4. Compute Paths */
		ArrayList<MSoaPath> paths = mbuilder.computePaths(vDsp, eDsp, sourcesIndex);
	
		/* 5. Identify sinks */
		for (MSoaPath path : paths) {
			sinksIndex.add(path.getSink());
		}
		
	    MSoaGraphBuilder m = mbuilder.createExternalTopology(
				vDsp,
				eDsp, 
				sourcesIndex,
				sinksIndex,
				paths);
	    m.setCandidate(r);
	    m.printGraph();
	    
	    if(weightAvailability==0.0 && weightRespTime == 1.0){
	    	try{ OptimalSolution op = obtainTMin(
	    			r,
					m,
					params
					);
    		System.out.println(op.toString());
			System.out.println(op.getSolution(r));
			System.out.println(op.getSolutionVertex(r));
			System.out.println(op.getSolutionEdge(r));
	    	}catch (ODDException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    else if(weightAvailability==1.0 && weightRespTime == 0.0){
	    	try {
	    	OptimalSolution op = obtainAMax(
	    			r,
					m,
					params
					);
    		System.out.println(op.toString());
			System.out.println(op.getSolution(r));
			System.out.println(op.getSolutionVertex(r));
			System.out.println(op.getSolutionEdge(r));
			}catch (ODDException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    else{
		    try {
		    	double tmax = obtainTMax(
		    			r,
						m,
						params
						).getOptR();
		    	
		    	double tmin = obtainTMin(
		    			r,
						m,
						params
						).getOptR();
		    	
		    	double amin = obtainAMin(
		    			r,
						m,
						params
						).getOptLogA();
		    	
		    	double amax = obtainAMax(
		    			r,
						m,
						params
						).getOptLogA();
		    	
		    	if(amax>amin){
			    	System.out.println(amax +" "+ amin +" "+ tmax +" "+ tmin);
			    	
			    	OptimalSolution op = optimalMSOAOrchestrationResolverV2(
			    			weightAvailability,
			    			weightRespTime,
							r,
							m,
							params,
							amax, 
							amin,
							tmax,
							tmin
							);
			    	
					System.out.println(op.toString());
					System.out.println(op.getSolution(r));
					System.out.println(op.getSolutionVertex(r));
					System.out.println(op.getSolutionEdge(r));
				}
		    	else{
		    		OptimalSolution op = obtainTMin(
			    			r,
							m,
							params
							);
		    		System.out.println(op.toString());
					System.out.println(op.getSolution(r));
					System.out.println(op.getSolutionVertex(r));
					System.out.println(op.getSolutionEdge(r));
		    	}
		    	
			} catch (ODDException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	}
    private static void testMSOBasic(){
    	/* ******* PARAMETERS ********** */
    	int NRES = 9;
    	double RESTRICTION_ON_VRES 		= -1;
    	int NDSP = 5; 
    	MSoaGraphBuilder.TYPE msoaType = MSoaGraphBuilder.TYPE.CUSTOM;
    	ResGraphBuilder.TYPE resType = ResGraphBuilder.TYPE.CUSTOM;
    	
    	try {
    		compileSolveAndReport("testMSO", 
    				1, 
    				1, 
    				NRES, 
    				resType, 
    				NDSP, 
    				msoaType, 
    				RESTRICTION_ON_VRES);
    	} catch (ODDException e) {
    		e.printStackTrace();
    	}

    }
    
    public static OptimalSolution optimalMSOAOrchestrationResolver(
    		double  weightAvailability,
    		double  weightRespTime,
    		ResGraphBuilder rbuilder,
    		MSoaGraphBuilder gbuilder,
    		ODDParameters params
    		) throws ODDException{
    	/* ******* PARAMETERS ********** */
    	/* ODD Weights */
    	WEIGHT_AVAILABILITY = weightAvailability;
    	WEIGHT_RESP_TIME =  weightRespTime;
    	
    	ODDModel model = new ODDServiceModel(
				gbuilder.getGraph(), rbuilder.getServiceGraph(), params);
    	System.out.println(model.toString());
    	OptimalSolution solution = model.solve();
    	return solution;
    	
    }
    
    public static OptimalSolution optimalMSOAOrchestrationResolverV2(
    		double  weightAvailability,
    		double  weightRespTime,
    		ResGraphBuilder rbuilder,
    		MSoaGraphBuilder gbuilder,
    		ODDParameters params,
    		double amax, 
			double amin,
			double tmax,
			double tmin
    		) throws ODDException{
    	/* ******* PARAMETERS ********** */
    	/* ODD Weights */
    	WEIGHT_AVAILABILITY = weightAvailability;
    	WEIGHT_RESP_TIME =  weightRespTime;
    	params.setWeightAvailability(WEIGHT_AVAILABILITY);
    	params.setWeightRespTime(WEIGHT_RESP_TIME);
    	params.setAmax(amax);
    	params.setAmin(amin);
    	params.setRmax(tmax);
    	params.setRmin(tmin);
    	ODDModel model = new ODDServiceModelV2(
				gbuilder.getGraph(), rbuilder.getServiceGraph(), params);
    	System.out.println(model.toString());
    	OptimalSolution solution = model.solve();
    	return solution;
    	
    	/*
    	ODDServiceModelV2 modelTMax = new ODDServiceModelV2(
				gbuilder.getGraph(), rbuilder.getServiceGraph(), params);
    	System.out.println(modelTMax.toString());
    	OptimalSolution solutionTMax = modelTMax.FindTmax();
    	return solutionTMax;
    	*/
    	
    	/*
    	ODDServiceModelV2 modelTMin = new ODDServiceModelV2(
				gbuilder.getGraph(), rbuilder.getServiceGraph(), params);
    	System.out.println(modelTMin.toString());
    	OptimalSolution solutionTMin = modelTMin.FindTmin();
    	return solutionTMin;
    	*/
    	
    	/*
    	ODDServiceModelV2 modelAMax = new ODDServiceModelV2(
				gbuilder.getGraph(), rbuilder.getServiceGraph(), params);
    	System.out.println(modelAMax.toString());
    	OptimalSolution solutionAMax = modelAMax.FindAmax();
    	return solutionAMax;
    	*/
    	
    	/*ODDServiceModelV2 modelAMin = new ODDServiceModelV2(
				gbuilder.getGraph(), rbuilder.getServiceGraph(), params);
    	System.out.println(modelAMin.toString());
    	OptimalSolution solutionAMin = modelAMin.FindAmin();
    	return solutionAMin;*/
    }

    public static OptimalSolution obtainTMin(
    		ResGraphBuilder rbuilder,
    		MSoaGraphBuilder gbuilder,
    		ODDParameters params
    		) throws ODDException{
    	/* ******* PARAMETERS ********** */
    	/* ODD Weights */
    	params.setWeightAvailability(0);
    	params.setWeightRespTime(1);
    	ODDServiceModelV2 modelTMin = new ODDServiceModelV2(
				gbuilder.getGraph(), rbuilder.getServiceGraph(), params);
    	System.out.println(modelTMin.toString());
    	OptimalSolution solutionTMin = modelTMin.FindTmin();
    	return solutionTMin;   	
    }
    public static OptimalSolution obtainTMax(
    		ResGraphBuilder rbuilder,
    		MSoaGraphBuilder gbuilder,
    		ODDParameters params
    		) throws ODDException{
    	/* ******* PARAMETERS ********** */
    	/* ODD Weights */
    	params.setWeightAvailability(0);
    	params.setWeightRespTime(1);
    	ODDServiceModelV2 modelTMax = new ODDServiceModelV2(
				gbuilder.getGraph(), rbuilder.getServiceGraph(), params);
    	System.out.println(modelTMax.toString());
    	OptimalSolution solutionTMax = modelTMax.FindTmax();
    	return solutionTMax;
    	
    }
    
    
    public static OptimalSolution obtainAMin(
    		ResGraphBuilder rbuilder,
    		MSoaGraphBuilder gbuilder,
    		ODDParameters params
    		) throws ODDException{
    	/* ******* PARAMETERS ********** */
    	/* ODD Weights */
    	params.setWeightAvailability(1);
    	params.setWeightRespTime(0);
    	ODDServiceModelV2 modelAMin = new ODDServiceModelV2(
				gbuilder.getGraph(), rbuilder.getServiceGraph(), params);
    	System.out.println(modelAMin.toString());
    	OptimalSolution solutionAMin = modelAMin.FindAmin();
    	return solutionAMin;
    }
    
    public static OptimalSolution obtainAMax(
    		ResGraphBuilder rbuilder,
    		MSoaGraphBuilder gbuilder,
    		ODDParameters params
    		) throws ODDException{
    	/* ******* PARAMETERS ********** */
    	/* ODD Weights */
    	params.setWeightAvailability(1);
    	params.setWeightRespTime(0);
    	ODDServiceModelV2 modelAMax = new ODDServiceModelV2(
				gbuilder.getGraph(), rbuilder.getServiceGraph(), params);
    	System.out.println(modelAMax.toString());
    	OptimalSolution solutionAMax = modelAMax.FindAmax();
    	return solutionAMax;
    }
    
    private static void compileSolveAndReport(    		
			String idSeries, 
			int experiment, 
			int runNumber, 
			int NRES, 
			ResGraphBuilder.TYPE resType, 
			int NDSP, 
			MSoaGraphBuilder.TYPE msoaType, 
			double RESTRICTION_ON_VRES) throws ODDException {
		
		
		System.out.println("Running " + idSeries + ": experiment: "
				+ experiment + ", run: " + runNumber);
		
		ODDParameters params = new ODDParameters(MODE.BASIC);
		params.setWeightAvailability(WEIGHT_AVAILABILITY);
		params.setWeightRespTime(WEIGHT_RESP_TIME);
		//params.setWeightCost(0);
		//params.setWeightNetMetric(0);
		setParameters(params);
		//params.setServiceRate(NOT_USED_INVERSE_REF_SERVICE_TIME);
		
		ResGraphBuilder rbuilder = new ResGraphBuilder();
		ResourceMetricsProvider rmp = new SimpleResourceMetricsProvider(params);
		rbuilder.create(rmp, resType, NRES);
		//rbuilder.printGraph(true);
		rbuilder.printSerGraph(true);
		
		MSoaGraphBuilder gbuilder = new MSoaGraphBuilder();
		ApplicationMetricsProvider amp = new SimpleApplicationMetricsProvider(msoaType, NDSP, params);
		gbuilder.create(amp, msoaType, NDSP);
		if (RESTRICTION_ON_VRES != -1)
			gbuilder.restrictPlacement(rbuilder.getGraph(), RESTRICTION_ON_VRES);
		gbuilder.setCandidate(rbuilder);
		gbuilder.printGraph();
		
		ODDModel model = new ODDServiceModel(
				gbuilder.getGraph(), rbuilder.getServiceGraph(), params);
		System.out.println(model.toString());
		OptimalSolution solution = model.solve();
		
		Report report = new Report(""+experiment, "output");
		try {
			report.write(idSeries, 
					runNumber, 
					model, 
					gbuilder, 
					amp, 
					rbuilder, 
					rmp, 
					RESTRICTION_ON_VRES, 
					params, 
					solution);
		} catch (ReportException | IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("+ Res time: compilation " + solution.getCompilationTime() + "ms, resolution " + solution.getResolutionTime() +" ms.");
		System.out.println("\n\n");
	}
    
    
    public static void setParameters(ODDParameters params){

		/* Application Metrics Provider */
		params.setRespTimeMin(3.0);
		params.setRespTimeMean(0.0); 
		params.setRespTimeStdDev(0.0); 
		

		params.setAvgBytePerTupleMin(1500.0);
		params.setAvgBytePerTupleMean(0.0); 
		params.setAvgBytePerTupleStdDev(0.0);

		params.setLambdaMin(0.014 / 5.0); // 99.978; // 10.0; // 99.978; // 100.0;
		params.setLambdaMean(0.0);
		params.setLambdaStdDev(0.0);
		params.setLambdaSheddingFactor(0); // 0.05; // 0.00005; // 0.15; // 0.0005;
		
		params.setReqResourcesMin(1.0);
		params.setReqResourcesMean(0.0);
		params.setReqResourcesStdDev(0.0);

		params.setCostPerResource(1.0);
		
		
		/* Resource Metrics Provider */
		params.setLinkDelayMin(1.0);
		params.setLinkDelayMean(22.0);
		params.setLinkDelayStdDev(5.0);

		params.setLinkAvailMin(1.0);
		params.setLinkAvailMean(0.0);
		params.setLinkAvailStdDev(0.0);

		params.setLinkBandwidthMin(Double.MAX_VALUE);
		params.setLinkBandwidthMean(0.0);
		params.setLinkBandwidthStdDev(0.0);

		params.setLinkCostPerUnitData(0.02);
		
		params.setNodeAvailResourcesMin(2.0);
		params.setNodeAvailResourcesMean(0.0);
		params.setNodeAvailResourcesStdDev(0.0);

		params.setNodeSpeedupMin(1.0);
		params.setNodeSpeedupMean(0.0);
		params.setNodeSpeedupStdDev(0.0);

		params.setNodeAvailabilityMin(.85);
		params.setNodeAvailabilityMean(.95);
		params.setNodeAvailabilityStdDev(0.03);
		
		params.setServiceRate(0.02); // 100.0; // 15.0; // 100.0
		
	}

	/* ******* PARAMETERS ********** */
	/* ODD Weights */
	private static double WEIGHT_AVAILABILITY = 0;
	private static double WEIGHT_RESP_TIME = 1;
	
	public static OptimalSolution optimalMSOAOrchestrationResolverV3(
    		double  weightAvailability,
    		double  weightRespTime,
    		ResGraphBuilder rbuilder,
    		MSoaGraphBuilder gbuilder,
    		ODDParameters params,
    		double amax, 
			double amin,
			double tmax,
			double tmin
    		) throws ODDException{
    	/* ******* PARAMETERS ********** */
    	/* ODD Weights */
    	WEIGHT_AVAILABILITY = weightAvailability;
    	WEIGHT_RESP_TIME =  weightRespTime;
    	params.setWeightAvailability(WEIGHT_AVAILABILITY);
    	params.setWeightRespTime(WEIGHT_RESP_TIME);
    	params.setAmax(amax);
    	params.setAmin(amin);
    	params.setRmax(tmax);
    	params.setRmin(tmin);
    	ODDModel model = new ODDServiceModelV3(
				gbuilder.getGraph(), rbuilder.getServiceGraph(), params);
    	System.out.println(model.toString());
    	OptimalSolution solution = model.solve();
    	return solution;
    	
    	/*
    	ODDServiceModelV2 modelTMax = new ODDServiceModelV2(
				gbuilder.getGraph(), rbuilder.getServiceGraph(), params);
    	System.out.println(modelTMax.toString());
    	OptimalSolution solutionTMax = modelTMax.FindTmax();
    	return solutionTMax;
    	*/
    	
    	/*
    	ODDServiceModelV2 modelTMin = new ODDServiceModelV2(
				gbuilder.getGraph(), rbuilder.getServiceGraph(), params);
    	System.out.println(modelTMin.toString());
    	OptimalSolution solutionTMin = modelTMin.FindTmin();
    	return solutionTMin;
    	*/
    	
    	/*
    	ODDServiceModelV2 modelAMax = new ODDServiceModelV2(
				gbuilder.getGraph(), rbuilder.getServiceGraph(), params);
    	System.out.println(modelAMax.toString());
    	OptimalSolution solutionAMax = modelAMax.FindAmax();
    	return solutionAMax;
    	*/
    	
    	/*ODDServiceModelV2 modelAMin = new ODDServiceModelV2(
				gbuilder.getGraph(), rbuilder.getServiceGraph(), params);
    	System.out.println(modelAMin.toString());
    	OptimalSolution solutionAMin = modelAMin.FindAmin();
    	return solutionAMin;*/
    }
	
	public static OptimalSolution obtainTMinV3(
    		ResGraphBuilder rbuilder,
    		MSoaGraphBuilder gbuilder,
    		ODDParameters params
    		) throws ODDException{
    	/* ******* PARAMETERS ********** */
    	/* ODD Weights */
    	params.setWeightAvailability(0);
    	params.setWeightRespTime(1);
    	ODDServiceModelV3 modelTMin = new ODDServiceModelV3(
				gbuilder.getGraph(), rbuilder.getServiceGraph(), params);
    	System.out.println(modelTMin.toString());
    	OptimalSolution solutionTMin = modelTMin.FindTmin();
    	return solutionTMin;   	
    }
    public static OptimalSolution obtainTMaxV3(
    		ResGraphBuilder rbuilder,
    		MSoaGraphBuilder gbuilder,
    		ODDParameters params
    		) throws ODDException{
    	/* ******* PARAMETERS ********** */
    	/* ODD Weights */
    	params.setWeightAvailability(0);
    	params.setWeightRespTime(1);
    	ODDServiceModelV3 modelTMax = new ODDServiceModelV3(
				gbuilder.getGraph(), rbuilder.getServiceGraph(), params);
    	System.out.println(modelTMax.toString());
    	OptimalSolution solutionTMax = modelTMax.FindTmax();
    	return solutionTMax;
    	
    }
    
    
    public static OptimalSolution obtainAMinV3(
    		ResGraphBuilder rbuilder,
    		MSoaGraphBuilder gbuilder,
    		ODDParameters params
    		) throws ODDException{
    	/* ******* PARAMETERS ********** */
    	/* ODD Weights */
    	params.setWeightAvailability(1);
    	params.setWeightRespTime(0);
    	ODDServiceModelV3 modelAMin = new ODDServiceModelV3(
				gbuilder.getGraph(), rbuilder.getServiceGraph(), params);
    	System.out.println(modelAMin.toString());
    	OptimalSolution solutionAMin = modelAMin.FindAmin();
    	return solutionAMin;
    }
    
    public static OptimalSolution obtainAMaxV3(
    		ResGraphBuilder rbuilder,
    		MSoaGraphBuilder gbuilder,
    		ODDParameters params
    		) throws ODDException{
    	/* ******* PARAMETERS ********** */
    	/* ODD Weights */
    	params.setWeightAvailability(1);
    	params.setWeightRespTime(0);
    	ODDServiceModelV3 modelAMax = new ODDServiceModelV3(
				gbuilder.getGraph(), rbuilder.getServiceGraph(), params);
    	System.out.println(modelAMax.toString());
    	OptimalSolution solutionAMax = modelAMax.FindAmax();
    	return solutionAMax;
    }
	
}





